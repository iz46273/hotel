
export default function defaultRequestOptions(method, values) {
    var token = localStorage.getItem('token');
    console.log(token);

    var httpHeaders;

    if(token) {
        if (method == "GET" || method == undefined) {
            httpHeaders = { 
                headers: {
                    Authorization: "Bearer " + token,
                },   
            }
        }
        else if (method == "DELETE") {
            httpHeaders = {
                method: 'DELETE',
                headers: {
                    Authorization: "Bearer " + token,
                },   
            }
        }
        else if (method == "POST") {
            httpHeaders = { 
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: "Bearer " + token,
                },
                body: JSON.stringify(values)
            }
        }
    }

    return httpHeaders;
};