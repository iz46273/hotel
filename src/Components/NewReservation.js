import { Button, Form, Input, InputNumber, message, Result, Select, Space, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import ReactFlagsSelect from "react-flags-select";
import { TbUsers } from 'react-icons/tb';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import ReservationDetails from './ReservationDetails';

const { Option } = Select;

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

export default function App({room, startDate, endDate}) {

    const [form] = Form.useForm();
    const [pansions, setPansions] = useState([]);
    const [vipList, setVipList] = useState([]);
    const [adults, setAdults] = useState(1);
    const [children, setChildren] = useState(0);
    const [newGuest, setNewGuest] = useState(false);
    const [country, setCountry] = useState("HR");
    const [page, setPage] = useState(1);
    const [formValues, setFormValues] = useState();
    const [vip, setVip] = useState('NO');
    const currentUser = JSON.parse(localStorage.getItem("user"));
    const [receptionist, setReceptionist] = useState();

    useEffect(() => {
        fetchPansions();
        fetchVip();
        fetchUser();
    }, [])

    const fetchUser = () => {
        fetch('api/employees/' + currentUser.id, defaultRequestOptions())
        .then((response) => response.json())
        .then(rec => {
            setReceptionist(rec);
        });
    }

    const fetchPansions = () => {
        fetch('/api/pansions', defaultRequestOptions())
        .then((response) => response.json())
        .then(pansionList => {
            setPansions(pansionList);
        });
    }
    const fetchVip = () => {
        fetch('/api/vip', defaultRequestOptions())
        .then((response) => response.json())
        .then(vipList => {
            setVipList(vipList);
        });
    }

    const onFinish = (values) => {
        if (newGuest) {
            values.guest.country = country;

            var vipObject = vipList.find(x => x.id === vip);
            values.guest.vip = vipObject;
        }

        var pansion = pansions.find(x => x.id === values.pansion);
        values.pansion = pansion;

        values.checkInDate = startDate;
        values.checkOutDate = endDate;
        values.room = room;
        values.receptionist = receptionist;

        fetch('/api/guests/checkIfExists/' + values.guest.email, defaultRequestOptions())
        .then((response) => response.json())
        .then(check => {
            if (check && newGuest) {
                message.error("Guest with this email already exists!")
            }
            else if (!check && !newGuest) {
                message.error("Guest with this email does not exist!")
            }
            else if (check &&!newGuest) {
                fetch('/api/guests/getByEmail/' + values.guest.email, defaultRequestOptions())
                .then((response) => response.json())
                .then(guest => {
                    values.guest = guest;
                    setFormValues(values);
                    setPage(2);
                });
            }
            else {
                values.guest.id = undefined;
                setFormValues(values);
                setPage(2);
            }
        });
    };

    const onSwitchChange = (value) => {
        setNewGuest(value);
    }

    const goBack = (value) => {
        setPage(value);
    }

    return (
        <>
        { page == 3 ?
            <Result
                status="success"
                title="Success!"
                subTitle="Reservation is saved successfully."
            />
        : <></>}
        { page == 2 ? 
        <>
            <ReservationDetails values={formValues} newGuest={newGuest} goBack={goBack} />
        </> : <></>}
        { page == 1 ?
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', padding:'5px'}}>
                    <div>
                        <a style={{color:'white', marginLeft: '150px'}}>Room&nbsp;</a>
                        <a style={{border: "1px white solid", borderRadius: "5px", padding: "5px", color:'white', marginRight: '50px'}}>{room.id}</a>
                        <a style={{color:'white'}}>Check-in date&nbsp;</a>
                        <a style={{border: "1px white solid", borderRadius: "5px", padding: "5px", color:'white', marginRight: '50px'}}>{startDate}</a>
                        <a style={{color:'white'}}>Check-out date&nbsp;</a>
                        <a style={{border: "1px white solid", borderRadius: "5px", padding: "5px", color:'white'}}>{endDate}</a>
                    </div>
                </Space>
            </div>
            <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px'}}>

            <Form
                {...formItemLayout}
                form={form}
                name="reservation"
                requiredMark="optional"
                onFinish={onFinish}
                initialValues={{
                prefix: '86',
                vip: 'NO',
                pansion: 'NO'
                }}
                scrollToFirstError
            >
                <div style={{marginLeft: '50px', marginRight: '200px', border: '1px solid lightskyblue', borderRadius: '5px', padding: '15px'}}>
                    <a style={{color: 'black'}}>Guest:</a>
                    <div style={{marginLeft:'50px', marginTop: '20px'}}>
                        <a>Guest already exists&nbsp;&nbsp;&nbsp;</a>
                        <Switch value={newGuest} onChange={onSwitchChange} />
                        <a>&nbsp;&nbsp;&nbsp;New guest</a>
                    </div>
                    <br></br>
                    {newGuest ?
                    <>
                        <Form.Item name={["guest", "firstName"]} label="First name" rules={[
                            {
                            required: true,
                            message: 'First name cannot be empty.',
                            whitespace: true,
                            },
                        ]}>
                            <Input />
                        </Form.Item>

                        <Form.Item name={["guest", "lastName"]} label="Last name" rules={[
                            {
                            required: true,
                            message: 'Last name cannot be empty.',
                            whitespace: true,
                            },
                        ]}>
                            <Input />
                        </Form.Item>

                        <Form.Item name={["guest", "email"]} label="E-mail" rules={[
                            {
                            type: 'email',
                            message: 'The input is not valid e-mail.',
                            },
                            {
                            required: true,
                            message: 'E-mail cannot be empty.',
                            },
                        ]}>
                            <Input />
                        </Form.Item>

                        <Form.Item name={["guest", "passport"]} label="Passport" rules={[
                            {
                            required: true,
                            message: 'Passport number cannot be empty.',
                            },
                        ]}>
                            <Input />
                        </Form.Item>

                        <Form.Item label="Country" name={["guest", "country"]}>
                                <ReactFlagsSelect selected={country} onSelect={(code) => setCountry(code)} />
                        </Form.Item>
                
                        <Form.Item name={["guest", "phone"]} label="Phone Number">
                            <PhoneInput placeholder="Enter phone number" international countryCallingCodeEditable={false} />
                        </Form.Item>

                        <Form.Item label="VIP">
                            <Select value={vip} onChange={(code) => setVip(code)}>
                                {vipList ? 
                                    vipList.length ?
                                        vipList.map((vip) => (
                                            <Option value={vip.id}>{vip.id}</Option>
                                        ))
                                    :  <></>
                                : <></>}
                            </Select>
                        </Form.Item>
                    </>
                    :
                        <Form.Item name={["guest", "email"]} label="E-mail" rules={[
                            {
                            type: 'email',
                            message: 'The input is not valid e-mail.',
                            },
                            {
                            required: true,
                            message: 'E-mail cannot be empty.',
                            },
                        ]}>
                            <Input />
                        </Form.Item>
                    }
                </div><br></br>
                <Form.Item label="Number of adults" name="adults" rules={[{required: true, message: 'Number of adults cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />} defaultValue={1} min={1} max={room.capacity - children} value={adults} onChange={(code) => setAdults(code)} />
                </Form.Item>
                <Form.Item label="Number of children" name="children" rules={[{required: true, message: 'Number of children cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />} defaultValue={0} min={0} max={room.capacity - adults} value={children} onChange={(code) => setChildren(code)} />
                </Form.Item>
                <Form.Item label="Number of babies" name="babies" rules={[{required: true, message: 'Number of babies cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />} defaultValue={0} min={0} max={2} />
                </Form.Item>
                <Form.Item label="Meal plan" name="pansion" rules={[{required: true}]}>
                    <Select>
                        {pansions ? 
                            pansions.length ?
                                pansions.map((pansion) => (
                                    <Option value={pansion.id}>{pansion.name}</Option>
                                ))
                            :  <></>
                        : <></>}
                    </Select>
                </Form.Item>
                <Form.Item name={['receptionNote']} label="Notes for reception" rules={[{ required: false }]}>
                    <Input.TextArea placeholder="notes" />
                </Form.Item>
                <Form.Item name={['housekeepingNote']} label="Notes for housekeeping" rules={[{ required: false }]}>
                    <Input.TextArea placeholder="notes" />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit reservation
                </Button>
                </Form.Item>
            </Form>
            </div>
        </> : <></>
        }
    </>
    )
}