import { Button, InputNumber, Popconfirm, Radio, Space, Table, Tooltip } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FiRefreshCcw } from 'react-icons/fi';
import { IoCheckmarkOutline, IoCloseOutline } from 'react-icons/io5';
import { TbUsers } from 'react-icons/tb';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import UpdateRoom from './UpdateRoom';

  const doubleOptions = [
    {
        label: 'All',
        value: 'all',
      },
    {
      label: 'Double',
      value: 'double',
    },
    {
      label: 'Twin',
      value: 'twin',
    },
];

const cleanOptions = [
    {
        label: 'All',
        value: 'all',
      },
    {
      label: 'Clean',
      value: 'clean',
    },
    {
      label: 'Dirty',
      value: 'dirty',
    },
];

export default function AllRooms () {
    const isFirstRender = useRef(true);
    const [rooms, setRooms] = useState([]);
    const [dataSource, setDataSource] = useState([]);
    const [double, setDouble] = useState('all');
    const [capacity, setCapacity] = useState(2);
    const [clean, setClean] = useState('all');
    const [updateRooms, setUpdateRooms] = useState(false);
    const [selectedRoom, setSelectedRoom] = useState();

    useEffect(() => {
        fetchRooms();
        isFirstRender.current = false;
    }, [])

    useEffect(() => {
        if (!isFirstRender.current) {
          filterRooms();
        }
    }, [double, capacity, clean])

    useEffect(() => {
        var data = [];
        for (var room of rooms) {
            data.push(room);
        }
        setDataSource(data);
    }, [rooms])

    const fetchRooms = () => {
        fetch('/api/rooms', defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            setRooms(roomList);
        });
    }

    const filterRooms = () => {
        fetch('/api/rooms/filter?' + new URLSearchParams({
            from: undefined,
            to: undefined,
            capacity : capacity,
            type: double,
            clean: clean
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            setRooms(roomList);
        });
    }

    const columns = [
        {
          title: 'Id',
          dataIndex: 'id',
          key: 'id',
          sorter: (a, b) => a.id - b.id,
        },
        {
          title: 'Capacity',
          dataIndex: 'capacity',
          key: 'capacity',
          sorter: (a, b) => a.capacity - b.capacity,
        },
        {
          title: 'Clean',
          dataIndex: 'clean',
          key: 'clean',
          render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                { record.clean ? <IoCheckmarkOutline /> : <IoCloseOutline /> }
                </>
            ) : null,
        },
        {
            title: 'Open',
            dataIndex: 'open',
            key: 'open',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                { record.open ? <IoCheckmarkOutline /> : <IoCloseOutline /> }
                </>
            ) : null,
        },
        {
            title: 'Category',
            dataIndex: 'category',
            key: 'category',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                { record.category.name }
                </>
            ) : null,
        },
        {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                { record.type.name }
                </>
            ) : null,
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.id)}>
                  <a>Delete &nbsp;&nbsp;&nbsp;</a>
                </Popconfirm>
                <a onClick={() => handleUpdate(record.id)}> Update</a>
                </>
            ) : null,
        },
    ];

    const onDoubleChange = ({ target: { value } }) => {
        setDouble(value);
    };

    const onCleanChange = ({ target: { value } }) => {
        setClean(value);
    };

    const onCapacityChange = (value) => {
        setCapacity(value);
    }

    const handleDelete = (key) => {
        fetch('api/rooms/' + key, defaultRequestOptions("DELETE"))
        .then(() => {
            filterRooms();
        })
    };

    const handleUpdate = (key) => {
        for (var room of rooms) {
            if (room.id === key)
                setSelectedRoom(room);
        }
        setUpdateRooms(true);
    };

    const handleGoBack = (value) => {
        filterRooms();
        setUpdateRooms(value);
    };

    return (
        <>
        { updateRooms ? 
            <UpdateRoom selectedRoom={selectedRoom} goBack={handleGoBack} />
        : <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                    <InputNumber addonBefore={<TbUsers />} style={{width: "100px"}} min={1} max={10} value={capacity} onChange={onCapacityChange} />
                    <Radio.Group options={doubleOptions} onChange={onDoubleChange} value={double} optionType="button" />
                    <Radio.Group options={cleanOptions} onChange={onCleanChange} value={clean} optionType="button" />
                </Space>
                <Tooltip title="Refresh">
                    <Button type="primary" onClick={() => filterRooms} shape="circle" size="large" icon={<FiRefreshCcw />} style={{float: 'right', margin: '5px', background: 'white', color: 'lightgrey'}}/>
                </Tooltip>
            </div>
            <div style={{marginBottom:'500px', float: 'none'}}>
                <Table  size="small" dataSource={dataSource} columns={columns} />
            </div>
        </> }
        </>
    )
}