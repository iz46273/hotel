import { Button, Modal, Tabs } from 'antd';
import { useEffect, useRef, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import MaidWorkingListPdf from './MaidWorkingListPdf';
import { OneMaidWorkingList } from './OneMaidWorkingList';

const { TabPane } = Tabs;

export default function MaidWorkingList() {
    
    const isFirstRender = useRef(true);
    const [ allCleaning, setAllCleaning ] = useState([]);
    const [ maids, setMaids ] = useState([]);
    const [ pdfVisible, setPdfVisible ] = useState(false);
    const [ selectedMaid, setSelectedMaid ] = useState();

    useEffect(() => {
        fetchList();
        isFirstRender.current = false;
    }, [])

    useEffect(() => {
        if (maids.length > 0) {
            setSelectedMaid(maids[0]);
        }
    }, [maids])

    useEffect(() => {
        var maidList = [];
        console.log(allCleaning);
        for (var cleaning of allCleaning) {
            var check = true;
            for (var maid of maidList) {
                if (cleaning.maid.id === maid.id) {
                    check = false;
                }
            }
            if (check) {
                maidList.push(cleaning.maid);
            }
        }
        setMaids(maidList);
    }, [allCleaning])

    const fetchList = () => {
        fetch('/api/cleaning/getToday', defaultRequestOptions())
        .then((response) => response.json())
        .then(values => {
            console.log(values);
            setAllCleaning(values);
        });
    }

    const onTabChange = (key) => {
        console.log(key);
        for (var maid of maids) {
            console.log(maid);
            if (maid.id == key)
                setSelectedMaid(maid);
        }
    };

    return(
        <>
        <div style={{float:'none', width: '100vh'}}></div>
        <Button type="primary" onClick={() => setPdfVisible(true)} style={{float: 'right', marginRight:'50px'}}>
            Print sheet
        </Button>
        <Modal
            title="View maid worksheet"
            centered
            visible={pdfVisible}
            onOk={() => setPdfVisible(false)}
            onCancel={() => setPdfVisible(false)}
            width={1000}
        >
            <MaidWorkingListPdf maid={selectedMaid} allCleaning={allCleaning}/>
        </Modal>
        <Tabs type="card" onChange={onTabChange} style={{width: '100%'}}>
        { maids ? 
            maids.length ?
                maids.map((maid) => (
                    <>
                    <TabPane tab={maid.firstName + " " + maid.lastName} key={maid.id}>
                        <OneMaidWorkingList maid={maid} allCleaning={allCleaning} updateList={fetchList} />
                    </TabPane>
                    </>
                    ))
                :  <></>
            : <></> }
        </Tabs>
        </>
    )
}