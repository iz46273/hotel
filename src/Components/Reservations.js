import { Space } from 'antd';
import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import ReservationCard from './ReservationCard';

export default function App({view}) {

    const [reservations, setReservations] = useState([]);
      
    useEffect(() => {
        fetchReservations();
    }, [])

    const fetchReservations = () => {
        fetch('/api/reservations/today/' + view, defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setReservations(reservationList);
        });
    }

    return (
        <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', fontSize: '150%', color: 'white'}}>
                <div style={{marginLeft: '20px'}}>
                {   view == "arrival" ? <>Arrival</> :
                    view == "dueOut" ? <>Due out</> :
                    view == "stayover" ? <>Stayover</> : <></>
                }
                </div>                
            </Space>
        </div>
        <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none'}}>
            {reservations ? 
                reservations.length ?
                    reservations.map((reservation) => (
                        <ReservationCard reservation={reservation} view={view} refresh={fetchReservations} />
                    ))
                :  <p>There are no reservations matching your request.</p>
            : <p>Error loading reservations.</p>}
        </div>
        </>
    )
}