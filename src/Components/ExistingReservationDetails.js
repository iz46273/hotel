import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { RiArrowGoBackLine } from 'react-icons/ri';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import './ReservationDetails.css';

export default function ExistingReservationDetails({reservation, setPage}) {

    const [stayBasePrice, setStayBasePrice] = useState();
    const [adultRoomPrice, setAdultRoomPrice] = useState();
    const [childRoomPrice, setChildRoomPrice] = useState();

    useEffect(() => {
        fetchStayBasePrice();
    }, [])

    useEffect(() => {
        var adultRoomPrice = stayBasePrice * 0.15;
        var childRoomPrice = stayBasePrice * 0.15 / 2;

        setAdultRoomPrice(adultRoomPrice);
        setChildRoomPrice(childRoomPrice);

        var adultPrice = reservation.pansion.price + adultRoomPrice;
        var childPrice = (reservation.pansion.price / 2) + childRoomPrice;

        var price = stayBasePrice + (adultPrice * reservation.adults) + (childPrice * reservation.children);
        reservation.totalPrice = price;
    }, [stayBasePrice])

    const fetchStayBasePrice = () => {
        fetch('/api/rooms/getStayPrice?roomId=' + reservation.room.id + '&from=' + reservation.checkInDate + '&to=' + reservation.checkOutDate, defaultRequestOptions())
            .then((response) => response.json())
            .then(price => {
                setStayBasePrice(price);
            }
        );
    }

    return(
        <>
        <div style={{float: 'none', width: '75%', height:'40px', background: 'lightskyblue', marginBottom: '15px', marginLeft: '100px', textAlign: 'center'}}>
            <h2 style={{color: 'white', marginLeft:'20px'}}>Reservation summary</h2>
        </div>
        <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px'}}>
            <table class="myTable">
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd">Check-in date</td>
                    <td class="MyTd">{reservation.checkInDate}</td>
                </tr>
                <tr class="MyTr">
                <td class="MyTd"></td>
                    <td class="MyTd">Check-out date</td>
                    <td class="MyTd">{reservation.checkOutDate}</td>
                </tr>
                <tr className="MyTr">
                    <td class="MyTd" rowSpan={2} style={{backgroundColor:'rgba(230,247,255,255)'}}>Room</td>
                    <td class="MyTd">Room number</td>
                    <td class="MyTd">{reservation.room.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Category</td>
                    <td class="MyTd">{reservation.room.category.name}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowspan={7}  style={{backgroundColor:'rgba(230,247,255,255)'}}>Guest</td>
                    <td class="MyTd">First Name</td>
                    <td class="MyTd">{reservation.guest.firstName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Last Name</td>
                    <td class="MyTd">{reservation.guest.lastName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Email</td>
                    <td class="MyTd">{reservation.guest.email}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Country</td>
                    <td class="MyTd">{reservation.guest.country}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Passport number</td>
                    <td class="MyTd">{reservation.guest.passport}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Phone number</td>
                    <td class="MyTd">{reservation.guest.phone}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">VIP status</td>
                    <td class="MyTd">{reservation.guest.vip.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd">Receptionist</td>
                    <td class="MyTd">{reservation.receptionist.firstName} {reservation.receptionist.lastName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowspan={4}  style={{backgroundColor:'rgba(230,247,255,255)'}}>Meal plan</td>
                    <td class="MyTd">Name</td>
                    <td class="MyTd">{reservation.pansion.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">per adult price</td>
                    <td class="MyTd">{reservation.pansion.price} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">per child price</td>
                    <td class="MyTd">{reservation.pansion.price / 2} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>Total meal plan price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>{(reservation.pansion.price * reservation.adults) + ((reservation.pansion.price / 2) * reservation.children)} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowSpan={4} style={{backgroundColor:'rgba(230,247,255,255)'}}>Room price</td>
                    <td class="MyTd">Basic price</td>
                    <td class="MyTd">{stayBasePrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">+ per adult price</td>
                    <td class="MyTd">{adultRoomPrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">+ per child price</td>
                    <td class="MyTd">{childRoomPrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>Total room price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>{stayBasePrice + (adultRoomPrice * reservation.adults) + (childRoomPrice * reservation.children)} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)', fontSize: 'large'}}>Total price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)', fontSize: 'large'}}>{reservation.totalPrice} €</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}></td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd"><Button onClick={event => setPage('all')} style={{borderRadius: "5px"}}><RiArrowGoBackLine />&nbsp;Back</Button></td>
                    <td class="MyTd"></td>
                </tr>
            </table>
        </div>
        </>
    )
}