import { Button, Space, Tooltip } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FiRefreshCcw } from 'react-icons/fi';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import AllGuestsTable from './AllGuestsTable';
import UpdateGuest from './UpdateGuest';

export default function AllGuests() {

    const isFirstRender = useRef(true);
    const [guests, setGuests] = useState([]);
    const [page, setPage] = useState('all');
    const [selectedGuest, setSelectedGuest] = useState();

    useEffect(() => {
        if (!isFirstRender.current) {
          //filterGuests();
        }
    }, [])

    useEffect(() => {
        filterGuests();
    }, [page])
      
    useEffect(() => {
        fetchGuests();
        isFirstRender.current = false;
    }, [])

    const fetchGuests = () => {
        fetch('/api/guests', defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            setGuests(list);
        });
    }

    const filterGuests = () => {
        /*fetch('/api/guests/filter?' + new URLSearchParams({
            lala: "lala"
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(guestList => {
            setGuests(guestList);
        });*/
        fetchGuests();
    }

    return (
        <>
        { page == 'all' ?
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', fontSize: '150%', color: 'white'}}>
                    All guests
                </Space>
                <Tooltip title="Refresh">
                    <Button type="primary" onClick={filterGuests} shape="circle" size="large" icon={<FiRefreshCcw />} style={{float: 'right', margin: '5px', background: 'white', color: 'lightgrey'}}/>
                </Tooltip>
            </div>
            <AllGuestsTable guests={guests} setPage={setPage} refresh={filterGuests} setSelectedGuest={setSelectedGuest} />
        </>
        : page == 'update' ?
        <>
            <UpdateGuest selectedGuest={selectedGuest} setPage={setPage} />
        </>
        : <></>
        }
        </>
    )
}