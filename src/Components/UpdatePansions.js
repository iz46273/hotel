import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Space } from 'antd';
import React from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const UpdatePansions = ({formValues}) => {

    const onFinish = (values) => {
        console.log(values.pansions);
        for (var value of values.pansions) {
            console.log(value);
            try {
                fetch('api/pansions', defaultRequestOptions("POST", value))
                .then((response) => {
                    if (response.ok) {
                        message.success('Changes are saved successfuly.');
                    }
                    else {
                        message.error('Something went wrong. Please try again.')
                    }
                })
            } catch (err) {
                message.error('Cannot make connection to server. Please try again.')
            }
        }
    };

  return (
    <>
    <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
        <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
            <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Meal plans</div>
        </Space>
    </div>
    <Form name="dynamic_form_nest_item"
        onFinish={onFinish}
        autoComplete="off"
        initialValues={{ pansions: formValues }}>
      <Form.List name="pansions">
        {(fields, { add, remove }) => (
          <>
            {fields.map(({ key, name, ...restField }) => (
              <Space
                key={key}
                style={{
                  display: 'flex',
                  marginBottom: 8,
                }}
                align="baseline"
              >
                <Form.Item
                  {...restField}
                  name={[name, 'id']}
                  rules={[
                    {
                      required: true,
                      message: 'ID cannot be empty.',
                    },
                  ]}
                >
                  <Input placeholder="ID" />
                </Form.Item>
                <Form.Item
                  {...restField}
                  name={[name, 'price']}
                  rules={[
                    {
                      required: true,
                      message: 'Price cannot be empty.',
                    },
                  ]}
                >
                  <Input placeholder="Price" />
                </Form.Item>
                <MinusCircleOutlined onClick={() => remove(name)} />
              </Space>
            ))}
            <Form.Item>
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                Add category
              </Button>
            </Form.Item>
          </>
        )}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
    </>
  );
};

export default UpdatePansions;