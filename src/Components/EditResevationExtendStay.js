import { DatePicker, message, Button, Space, Alert } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import 'react-phone-number-input/style.css';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export default function EditReservationExtendStay({reservation, setPage}) {

    const [firstUnavailableDate, setFirstUnavailableDate] = useState();
    const [checkOutDate, setCheckOutDate] = useState();
    const [oldCheckOutDate, setOldCheckOutDate] = useState();
    const [stringDate, setStringDate] = useState();

    useEffect(() => {
        fetchUnavailableDates();
        const end = moment(reservation.checkOutDate);
        setOldCheckOutDate(end);
        setCheckOutDate(end);
    }, [])

    const disabledDate = (current) => {
        if (firstUnavailableDate > oldCheckOutDate)
            return current < oldCheckOutDate || current >= firstUnavailableDate;
        else
            return current < oldCheckOutDate;
    };

    const fetchUnavailableDates = () => {
        fetch('/api/rooms/getFirstUnavailableDate/' + reservation.room.id + '/' + reservation.checkOutDate, defaultRequestOptions())
        .then((response) => response.json())
        .then(date => {
            setFirstUnavailableDate(moment(date));
        });
    };

    const onChange = (date, dateString) => {
        setCheckOutDate(date);
        setStringDate(dateString);
    };

    const submit = () => {
        try {
            fetch('/api/reservations/updateCheckOutDate/' + reservation.id + '/' + stringDate, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    message.success('Changes are saved successfuly.');
                    setPage('all');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    return (
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', color: 'white', fontSize:'150%'}}>
                    Extend stay
                </Space>
            </div>
            <br></br>
            <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px', marginRight: '100px'}}>
                <Alert
                    message="Choose check-out date"
                    description="Choose a new check-out date from the offered available dates to extend the stay. If no dates are available, please try moving to another room."
                    type="info"
                    /><br></br>
                <table>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Room</td>
                        <td style={{padding: '10px'}}>{reservation.room.id}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Check-in date</td>
                        <td style={{padding: '10px'}}>{reservation.checkInDate}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Old check-out date</td>
                        <td style={{padding: '10px'}}>{reservation.checkOutDate}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>New check-out date</td>
                        <td style={{padding: '10px'}}><DatePicker
                            format="YYYY-MM-DD"
                            disabledDate={disabledDate}
                            value={checkOutDate}
                            onChange={onChange}
                            allowClear={false}
                            />
                        </td>
                    </tr><tr><td>&nbsp;</td></tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>
                        <Button type="default" onClick={() => setPage('all')}>
                            Go back
                        </Button>
                        </td>
                        <td style={{padding: '10px'}}>
                        <Button type="primary" onClick={() => submit()}>
                            Submit changes
                        </Button>
                        </td>
                    </tr>
                </table>
            </div>
        </>
    )
}