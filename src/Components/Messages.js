import { PlusOutlined } from '@ant-design/icons';
import {
    Button, Empty, Form,
    Input, message, Modal, Select, Tooltip
} from 'antd';
import React, { useEffect, useState } from 'react';
import { BsInfoCircle } from 'react-icons/bs';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import MessageCard from './MessageCard';

const { Option } = Select;
const { TextArea } = Input;



const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

const Messages = () => {
    const [messages, setMessages] = useState();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [employees, setEmployees] = useState([]);
    const [form] = Form.useForm();
    const currentUser = JSON.parse(localStorage.getItem("user"));

    const showModal = () => {
        setIsModalVisible(true);
    };
    
    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const fetchMessages = () => {
        fetch('/api/messages/' + currentUser.id, defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            setMessages(list);
        });
    }

    const fetchEmployees = () => {
        fetch('/api/employees', defaultRequestOptions())
        .then((response) => response.json())
        .then(employeeList => {
            var list = [];
            for (let employee of employeeList) {
                if (employee.id != currentUser.id && employee.username != undefined)
                    list.push(employee);
            }
            setEmployees(list);
        });
    }

    useEffect(() => {
        fetchEmployees();
        fetchMessages();
    }, [])

    const newMessage = (message) => {
        showModal();
        if (message) {
            form.setFieldsValue({
                to: message.from.id,
                seen: message.id
            })
        }
    }

    const setSeen = (id) => {
        try {
            fetch('api/messages/read/' + id, defaultRequestOptions())
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
        fetchMessages();
    }

    const onFinish = (values) => {
        var emp = employees.find(x => x.id === values.to);
        values.to = emp;
        values.from = currentUser;
        values.confirmed = false;
        try {
            fetch('api/messages', defaultRequestOptions("POST", values))
            .then((response) => {
                if (response.ok) {
                    setSeen(values.seen);
                    message.success('Message sent.');
                    handleCancel();
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    return(
        <>
        <a style={{color: "rgba(24,144,255,255)", fontSize: '150%'}}>Messages</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Button onClick={() => newMessage(undefined)}><PlusOutlined />New</Button><br></br><br></br>
        {messages ? 
            messages.length ?
                messages.map((message) => (
                    <MessageCard myMessage={message} newMessage={newMessage} refresh={fetchMessages} />
                ))
            :  <><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No new messages" /></>
                : <></>}    
        <Modal title="Send new message" visible={isModalVisible} footer={null} width={600}>
        <Form
            form={form}
            size="small"
            onFinish={onFinish}
            >
            <Form.Item label="Receiver" name="to" rules={[{required: true}]}>
                <Select style={{width: '150px'}}>
                    {employees ? 
                        employees.length ?
                            employees.map((emp) => (
                                <Option value={emp.id}>
                                    <Tooltip title={emp.position}>
                                        <BsInfoCircle />
                                    </Tooltip>
                                    &nbsp;&nbsp;{emp.firstName} {emp.lastName}
                                </Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
            </Form.Item>
            <Form.Item label="Message" name="message" rules={[{required: true}]}>
                <TextArea  style={{width: '85%'}} rows={4} />
            </Form.Item>
            <Form.Item name="seen">
                <input type="hidden"></input>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="default" onClick={() => handleCancel()}>Cancel</Button>&nbsp;&nbsp;
                <Button type="primary" htmlType="submit">Send</Button>
            </Form.Item>
            </Form>
        </Modal>    
        </>
    )
}

export default Messages;