import { Button, Popconfirm, Popover, Table, message } from 'antd';
import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export default function AllReservationsTable({ reservations, refresh, setSelectedReservation, setPage }) {

    const [dataSource, setDataSource] = useState();

    const columns = [
        {
          title: 'Room',
          dataIndex: 'roomId',
          key: 'roomId',
        },
        {
          title: 'Status',
          dataIndex: 'status',
          key: 'status',
        },
        {
            title: 'Guest',
            dataIndex: 'guest',
            key: 'guest',
        },
        {
            title: 'Check-in date',
            dataIndex: 'checkInDate',
            key: 'checkInDate',
        },
        {
            title: 'Check-in time',
            dataIndex: 'checkInTime',
            key: 'checkInTime',
        },
        {
            title: 'Check-out date',
            dataIndex: 'checkOutDate',
            key: 'checkOutDate',
        },
        {
            title: 'Check-out time',
            dataIndex: 'checkOutTime',
            key: 'checkOutTime',
        },
        {
            title: 'Guests',
            dataIndex: 'guests',
            key: 'guests',
        },
        {
            title: 'Notes',
            key: 'notes',
            render : (_, record) => (<>
                <Popover content={() => record.housekeepingNote} title="Housekeeping note">
                    <Button size='small'>Hsk</Button>&nbsp;
                </Popover>
                <Popover content={() => record.receptionNote} title="Reception note">
                    <Button size='small'>Rcp</Button>
                </Popover>
                </>
            )
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                <a onClick={() => handleDetails(record.key)}>Details &nbsp;</a>
                <Popover
                    content={
                    <>
                        <a onClick={() => handleUpdate(record.key)}>Update reservation details</a><br></br>
                        <a onClick={() => handleExtend(record.key)}>Extend the stay</a><br></br>
                        <a onClick={() => handleShorten(record.key)}>Shorten the stay</a><br></br>
                        <a onClick={() => handleMove(record.key)}>Move to another room</a><br></br>
                    </>
                    }
                    title="Edit options"
                    trigger="click"
                    >
                    <a>Edit &nbsp;</a>
                </Popover>
                <Popconfirm title="Sure to cancel?" onConfirm={() => handleCancel(record.key)}>
                  <a>Cancel</a>
                </Popconfirm>
                </>
            ) : null,
        },
    ];
      
    useEffect(() => {
        var data = [];
        for (var reservation of reservations) {
            var key = reservation.id;
            var roomId = reservation.room.id;
            var status = reservation.status;
            var guest = reservation.guest.firstName + " " + reservation.guest.lastName;
            var checkInDate = reservation.checkInDate;
            var checkInTime = reservation.checkInTime;
            var checkOutDate = reservation.checkOutDate;
            var checkOutTime = reservation.checkOutTime;
            var guests = reservation.adults + reservation.children + reservation.babies;
            var housekeepingNote = '/';
            var receptionNote = '/';
            if (reservation.housekeepingNote)
                housekeepingNote = reservation.housekeepingNote;
            if (reservation.receptionNote)
                receptionNote = reservation.receptionNote;
            var row = {
                key: key,
                roomId: roomId,
                status: status,
                guest: guest,
                checkInDate: checkInDate,
                checkInTime: checkInTime,
                checkOutDate: checkOutDate,
                checkOutTime: checkOutTime,
                guests: guests,
                housekeepingNote: housekeepingNote,
                receptionNote: receptionNote,
            };
            data.push(row);
        }
        setDataSource(data);
    }, [reservations])

    const handleCancel = (key) => {
        console.log(key);
        try {
            fetch('api/reservations/cancel/' + key, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    refresh();
                    message.success('Reservation canceled successfully.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    };

    const handleUpdate = (key) => {
        for (var res of reservations) {
            if (res.id === key)
                setSelectedReservation(res);
        }
        setPage('update');
    };

    const handleExtend = (key) => {
        for (var res of reservations) {
            if (res.id === key)
                setSelectedReservation(res);
        }
        setPage('extend');
    };

    const handleShorten = (key) => {
        for (var res of reservations) {
            if (res.id === key)
                setSelectedReservation(res);
        }
        setPage('shorten');
    };

    const handleMove = (key) => {
        for (var res of reservations) {
            if (res.id === key)
                setSelectedReservation(res);
        }
        setPage('move');
    };

    const handleDetails = (key) => {
        for (var res of reservations) {
            if (res.id === key)
                setSelectedReservation(res);
        }
        setPage('details');
    };

    return (
        <>
            <Table dataSource={dataSource} columns={columns} size="small" />
        </>
    )
}