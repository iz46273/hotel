import { Button, Card } from 'antd';
import React from 'react';
import { HiPlus } from 'react-icons/hi';
import { IoMdBed } from 'react-icons/io';
import { IoBed } from 'react-icons/io5';
import { MdOutlineCleaningServices } from 'react-icons/md';
import { TbUsers } from 'react-icons/tb';

export default function RoomCard ({room, dates, handleSelectRoom}) {

    const newReservation = () => {
        if (dates) {
            return(
                <Button onClick={event => handleSelectRoom({room})} style={{borderRadius: "5px"}}><HiPlus />&nbsp;Reservation</Button>
            )
        }
        else {
            return(
                <></>
            )
        }
    }

    return (
        <Card
            size="small"
            title={room.id}
            extra={newReservation()}
            style={{
            border: 'solid 1px lightskyblue',
            width: 300,
            float: 'left',
            margin: '10px',
            boxShadow: '5px 5px 5px #aaaaaa',
            fontFamily: 'Arial',
            fontWeight: 'light',
            }}
        >
            <table style={{width: '100%', textAlign: 'left', marginLeft: '20px', marginRight: '20px'}}>
                <tr>
                    <td style={{fontSize: '120%', padding: '5px'}}>{room.category.name}</td>
                    { room.type.name == 'double' ?
                    <td style={{padding:'5px'}}><IoBed size='15px'/> {room.type.name}</td>
                    :
                    <td style={{padding:'5px'}}><IoMdBed size='15px' /> {room.type.name}</td>
                    }
                </tr>
                <tr>
                    <td style={{padding:'5px'}}><TbUsers /> {room.capacity}</td>
                    { room.clean ?
                    <td style={{padding:'5px'}}><MdOutlineCleaningServices size='15px'/> clean</td>
                    :
                    <td style={{padding:'5px'}}><MdOutlineCleaningServices size='15px'/> dirty</td>
                    }
                </tr>
            </table>
        </Card>
    )
}