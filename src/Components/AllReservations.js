import { Button, Space, Tooltip } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FiRefreshCcw } from 'react-icons/fi';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import AllReservationsTable from './AllReservationsTable';
import EditReservationMoveToAnotherRoom from './EditReservationMoveToAnotherRoom';
import EditReservationShortenStay from './EditReservationShortenStay';
import EditReservationExtendStay from './EditResevationExtendStay';
import ExistingReservationDetails from './ExistingReservationDetails';
import UpdateReservation from './UpdateReservation';

export default function AllReservations() {

    const isFirstRender = useRef(true);
    const [reservations, setReservations] = useState([]);
    const [page, setPage] = useState('all');
    const [selectedReservation, setSelectedReservation] = useState();

    useEffect(() => {
        if (!isFirstRender.current) {
          //filterReservations();
        }
    }, [])

    useEffect(() => {
        filterReservations();
    }, [page])
      
    useEffect(() => {
        fetchReservations();
        isFirstRender.current = false;
    }, [])

    const fetchReservations = () => {
        fetch('/api/reservations', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setReservations(reservationList);
        });
    }

    const filterReservations = () => {
        /*fetch('/api/reservations/filter?' + new URLSearchParams({
            lala: "lala"
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setReservations(reservationList);
        });*/
        fetchReservations();
    }

    return (
        <>
        { page == 'all' ?
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', fontSize: '150%', color: 'white'}}>
                    All reservations
                </Space>
                <Tooltip title="Refresh">
                    <Button type="primary" onClick={filterReservations} shape="circle" size="large" icon={<FiRefreshCcw />} style={{float: 'right', margin: '5px', background: 'white', color: 'lightgrey'}}/>
                </Tooltip>
            </div>
            <AllReservationsTable reservations={reservations} setPage={setPage} refresh={filterReservations} setSelectedReservation={setSelectedReservation} />
        </>
        : page == 'update' ?
        <>
            <UpdateReservation reservation={selectedReservation} setPage={setPage} />
        </>
        : page == 'extend' ?
        <>
            <EditReservationExtendStay reservation={selectedReservation} setPage={setPage} />
        </>
        : page == 'shorten' ?
        <>
            <EditReservationShortenStay reservation={selectedReservation} setPage={setPage} />
        </>
        : page == 'move' ?
        <>
            <EditReservationMoveToAnotherRoom reservation={selectedReservation} setPage={setPage} />
        </>
        : page == 'details' ?
        <>
            <ExistingReservationDetails reservation={selectedReservation} setPage={setPage} />
        </>
        : <></>
        }
        </>
    )
}