import { Checkbox, Divider, message } from 'antd';
import { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export function MaidCheckboxGroup({ maid, reservationsLeft, removeReservations, addReservations, save, handleCleaningGenerated }) {

    const [reservations, setReservations] = useState([]);
    const [cleaning, setCleaning] = useState([]);

    useEffect(() => {
        if (save) {
            try {
                fetch("/api/cleaning/new", defaultRequestOptions("POST", cleaning))
                .then((response) => {
                    if (response.ok) {
                        setTimeout(function(){
                            message.success('Rooms assigned successfully.');
                            handleCleaningGenerated(true);
                        }, 3000);
                    }
                    else {
                        message.error('Something went wrong. Please try again.')
                    }
                })
            } catch (err) {
                message.error('Cannot make connection to server. Please try again.')
            }
        }
    }, [save])

    useEffect(() => {
        let arr = [];
        for (let value of reservations) {
            var cleaning = {
                maid: maid,
                room: value.room,
                date: undefined
            }
            arr.push(cleaning);
        }
        setCleaning(arr);
    }, [reservations])

    const checkReservation = (checkedValues) => {
        removeReservations(checkedValues);

        const arr = [...reservations];
        arr.push(checkedValues);
        setReservations(arr);
    }

    const uncheckReservation = (checkedValues) => {
        addReservations(checkedValues);

        const arr = [...reservations];
        var filtered = reservations.filter(res => res != checkedValues);
        setReservations(filtered);
    }

    return(
        <>
        { reservations ? 
                reservations.length ?
                    <>
                    { reservations.map((reservation) => (
                        <><Checkbox checked={true} onChange={() => uncheckReservation(reservation)}>
                            { reservation.room.id }&nbsp;
                            { reservation.arrival ? <>!&nbsp;</> : <>&nbsp;&nbsp;</> }
                            { reservation.departure ? <>Departured&nbsp;</> : 
                                reservation.stayover ? <>Stayover&nbsp;</> : 
                                reservation.dueOut ? <>Due out&nbsp;</> :
                                reservation.vacant ? <>Vacant&nbsp;</> :
                                reservation.arrival ? <>Arrival&nbsp;</> : <></> }
                        </Checkbox><br></br></>
                    )) }
                    </>
                :   <>
                        <div>
                            No rooms selected.
                        </div>
                    </>
            : <></> }
        <>
        <Divider />
        <div style={{overflow: 'auto', height: '270px'}}>
            { reservationsLeft ? 
                reservationsLeft.length ?
                    reservationsLeft.map((reservation) => (
                        <><Checkbox checked={false} onChange={() => checkReservation(reservation)}>
                            { reservation.room.id }&nbsp;
                            { reservation.arrival ? <>!&nbsp;</> : <>&nbsp;&nbsp;</> }
                            { reservation.departure ? <>Departured&nbsp;</> : 
                                reservation.stayover ? <>Stayover&nbsp;</> : 
                                reservation.dueOut ? <>Due out&nbsp;</> :
                                reservation.vacant ? <>Vacant&nbsp;</> :
                                reservation.arrival ? <>Arrival&nbsp;</> : <></> }
                        </Checkbox><br></br></>
                    ))
                :  <></>
            : <></> }
        </div>
        </></>
    )
}