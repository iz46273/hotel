import { Button, Popconfirm, Radio, Space, Table, Tooltip } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FiRefreshCcw } from 'react-icons/fi';
import { IoCheckmarkOutline, IoCloseOutline } from 'react-icons/io5';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import UpdateEmployee from './UpdateEmployee';

const positionOptions = [
    {
        label: 'All',
        value: 'all',
    },
    {
      label: 'Housekeeping',
      value: 'HOUSEKEEPING',
    },
    {
      label: 'Reception',
      value: 'RECEPTION',
    },
    {
        label: 'Maid',
        value: 'MAID',
    },
    {
        label: 'Admin',
        value: 'ADMINISTRATION',
    },
];

const activeOptions = [
    {
        label: 'All',
        value: 'all',
    },
    {
      label: 'Active',
      value: 'true',
    },
    {
      label: 'Inactive',
      value: 'false',
    },
];

export default function AllEmployees () {
    const isFirstRender = useRef(true);
    const [employees, setEmployees] = useState([]);
    const [dataSource, setDataSource] = useState([]);
    const [position, setPosition] = useState('all');
    const [active, setActive] = useState('all');
    const [updateEmployees, setUpdateEmployees] = useState(false);
    const [selectedEmployee, setSelectedEmployee] = useState();

    useEffect(() => {
        fetchEmployees();
        isFirstRender.current = false;
    }, [])

    useEffect(() => {
        if (!isFirstRender.current) {
            filterEmployees();
        }
    }, [position, active])

    useEffect(() => {
        console.log(employees);
        var data = [];
        for (var emp of employees) {
            data.push(emp);
        }
        setDataSource(data);
    }, [employees])

    const fetchEmployees = () => {
        fetch('/api/employees', defaultRequestOptions())
        .then((response) => response.json())
        .then(employeeList => {
            setEmployees(employeeList);
        });
    }

    const filterEmployees = () => {
        fetch('/api/employees/filter?' + new URLSearchParams({
            position: position,
            active: active
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            setEmployees(list);
        });
    }

    const columns = [
        {
          title: 'Id',
          dataIndex: 'id',
          key: 'id',
          sorter: (a, b) => a.id - b.id,
        },
        {
            title: 'First name',
            dataIndex: 'firstName',
            key: 'firstName',
        },
        {
            title: 'Last name',
            dataIndex: 'lastName',
            key: 'lastName',
        },
        {
            title: 'Position',
            dataIndex: 'position',
            key: 'position',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
          title: 'Account created',
          dataIndex: 'accountCreated',
          key: 'accountCreated',
        },
        {
          title: 'Active',
          dataIndex: 'active',
          key: 'active',
          render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                { record.active ? <IoCheckmarkOutline /> : <IoCloseOutline /> }
                </>
            ) : null,
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>
                <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.id)}>
                  <a>Delete &nbsp;&nbsp;&nbsp;</a>
                </Popconfirm>
                <a onClick={() => handleUpdate(record.id)}> Update</a>
                </>
            ) : null,
        },
    ];

    const onPositionChange = ({ target: { value } }) => {
        setPosition(value);
    };

    const onActiveChange = ({ target: { value } }) => {
        setActive(value);
    };

    const handleDelete = (key) => {
        fetch('api/employees/' + key, defaultRequestOptions("DELETE"))
        .then(() => {
            filterEmployees();
        })
    };

    const handleUpdate = (key) => {
        for (var emp of employees) {
            if (emp.id === key)
                setSelectedEmployee(emp);
        }
        setUpdateEmployees(true);
    };

    const handleGoBack = (value) => {
        filterEmployees();
        setUpdateEmployees(value);
    };

    return (
        <>
        { updateEmployees ? 
            <UpdateEmployee selectedEmployee={selectedEmployee} goBack={handleGoBack} />
        : <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                    <Radio.Group options={positionOptions} onChange={onPositionChange} value={position} optionType="button" />
                    <Radio.Group options={activeOptions} onChange={onActiveChange} value={active} optionType="button" />
                </Space>
                <Tooltip title="Refresh">
                    <Button type="primary" onClick={() => filterEmployees} shape="circle" size="large" icon={<FiRefreshCcw />} style={{float: 'right', margin: '5px', background: 'white', color: 'lightgrey'}}/>
                </Tooltip>
            </div>
            <div style={{marginBottom:'500px', float: 'none'}}>
                <Table  size="small" dataSource={dataSource} columns={columns} />
            </div>
        </> }
        </>
    )
}