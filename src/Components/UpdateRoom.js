import {
    Button, Form, InputNumber, message, Select, Space
} from 'antd';
import React, { useEffect, useState } from 'react';

import { BsArrowLeft, BsArrowRight } from 'react-icons/bs';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

const { Option } = Select;

export default function UpdateRoom ({ selectedRoom, goBack }) {

    const [form] = Form.useForm();
    const [categories, setCategories] = useState([]);
    const [types, setTypes] = useState([]);

    useEffect(() => {
        fetchCategory();
        fetchType();
    }, [])

    const fetchCategory = () => {
        fetch('/api/categories', defaultRequestOptions())
        .then((response) => response.json())
        .then(catList => {
            console.log(catList);
            setCategories(catList);
        });
    }

    const fetchType = () => {
        fetch('/api/roomTypes', defaultRequestOptions())
        .then((response) => response.json())
        .then(typeList => {
            setTypes(typeList);
        });
    } 

    const onFinish = (values) => {
        values.id = selectedRoom.id;
        values.clean = selectedRoom.clean;

        var cat = categories.find(x => x.id === values.category);
        values.category = cat;

        var type = types.find(x => x.name === values.type);
        values.type = type;

        console.log(values);
        
        try {
            fetch('api/rooms/update', defaultRequestOptions("POST", values))
            .then((response) => {
                if (response.ok) {
                    message.success('Changes are saved successfuly.');
                    goBack(false);
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    };

    return (
        <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Room details: {selectedRoom.id}</div>
            </Space>
        </div>
        <Form
            {...formItemLayout}
            form={form}
            size="small"
            onFinish={onFinish}
            initialValues={{
                capacity: selectedRoom.capacity,
                category: selectedRoom.category.id,
                type: selectedRoom.type.name,
                open: selectedRoom.open,
            }}
            >
            <Form.Item label="Capacity" name="capacity" rules={[{required: true}]}>
                <InputNumber />
            </Form.Item>
            <Form.Item label="Category" name="category" rules={[{required: true}]}>
                <Select>
                    {categories ? 
                        categories.length ?
                            categories.map((categ) => (
                                <Option value={categ.id}>{categ.name}</Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
                </Form.Item>
            <Form.Item label="Open" name="open" rules={[{required: true}]}>
                <Select>
                    <Select.Option value={true}>Open</Select.Option>
                    <Select.Option value={false}>Closed</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item label="Type" name="type" rules={[{required: true}]}>
                <Select>
                    {types ? 
                        types.length ?
                            types.map((type) => (
                                <Option value={type.name}>{type.name}</Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="default" onClick={() => goBack(false)}>
                        <BsArrowLeft />&nbsp;Go back
                </Button>&nbsp;&nbsp;
                <Button type="primary" htmlType="submit">
                    Submit changes&nbsp;<BsArrowRight />
                </Button>
            </Form.Item>
            </Form>
        </>
    )
}