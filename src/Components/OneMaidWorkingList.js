import { Input, message, Select, Table } from 'antd';
import { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export function OneMaidWorkingList({maid, allCleaning, updateList}) {
    const { Option } = Select;

    const [cleaning, setCleaning] = useState([]);
    const [dataSource, setDataSource] = useState();

    useEffect(() => {
        var data = [];
        for (var cle of cleaning) {
            var key = cle.reservation.room.id;
            var room = cle.reservation.room;
            var roomId;
            if (cle.arrival)
                roomId = cle.reservation.room.id + " !";
            else
                roomId = cle.reservation.room.id;
            var status;
            if (cle.arrival)
                status = "Arrival";
            if (cle.dueOut)
                status = "Due Out";
            if (cle.departured)
                status = "Departured";
            if (cle.stayover)
                status = "Stayover";
            if (cle.vacant)
                status = "Vacant";
            if (cle.reservation.guest)
                var vip = cle.reservation.guest.vip.id;
            else
                var vip = undefined;
            var checkInDate = cle.reservation.checkInDate;
            var checkInTime = cle.reservation.checkInTime;
            var checkOutDate = cle.reservation.checkOutDate;
            var checkOutTime = cle.reservation.checkOutTime;
            var guests = cle.reservation.adults + cle.reservation.children + cle.reservation.babies;
            var note = cle.reservation.housekeepingNote;
            var maidNote = cle.maidNote;
            var row = {
                key: key,
                room: room,
                roomId: roomId,
                status: status,
                vip: vip,
                checkInDate: checkInDate,
                checkInTime: checkInTime,
                checkOutDate: checkOutDate,
                checkOutTime: checkOutTime,
                guests: guests,
                guestNote: note,
                maidNote, maidNote
            };
            data.push(row);
        }
        setDataSource(data);
    }, [cleaning])

    const columns = [
        {
          title: 'Room',
          dataIndex: 'roomId',
          key: 'roomId',
        },
        {
          title: 'Status',
          dataIndex: 'status',
          key: 'status',
        },
        {
            title: 'VIP',
            dataIndex: 'vip',
            key: 'vip',
        },
        {
            title: 'Check-in date',
            dataIndex: 'checkInDate',
            key: 'checkInDate',
        },
        {
            title: 'Check-in time',
            dataIndex: 'checkInTime',
            key: 'checkInTime',
        },
        {
            title: 'Check-out date',
            dataIndex: 'checkOutDate',
            key: 'checkOutDate',
        },
        {
            title: 'Check-out time',
            dataIndex: 'checkOutTime',
            key: 'checkOutTime',
        },
        {
            title: 'Guest num',
            dataIndex: 'guests',
            key: 'guests',
        },
        {
            title: 'Guest Note',
            dataIndex: 'guestNote',
            key: 'guestNote',
        },
        {
            title: 'Status',
            key: 'clean',
            fixed: 'right',
            width: 100,
            render : (_, record) => (
                <Select style={{width: '100px'}} defaultValue={record.room.clean} onSelect={(cleanChange)}>
                    <Option name={record.room.id} value={false}>Dirty</Option>
                    <Option name={record.room.id} value={true}>Clean</Option>
                </Select>
            )
        },
        {
            title: 'Note',
            key: 'maidNote',
            fixed: 'right',
            width: 100,
            render : (_, record) => (
                <Input style={{width: '100px'}} onChange={(noteChange)} allowClear name={record.room.id} defaultValue={record.maidNote}>
                </Input>
            )
        },
    ];

    useEffect(() => {
        var cleaningList = [];
        for (var cleaning of allCleaning) {
            if (cleaning.maid.id === maid.id) {
                cleaningList.push(cleaning);
            }
        }
        setCleaning(cleaningList);
    }, [])

    const noteChange = ({ target }) => {
        var cle = {};
        for (let obj of cleaning) {
            if (obj.reservation.room.id == target.name) {
                cle.room = obj.reservation.room;
                cle.maid = maid;
                cle.date = obj.date;
                cle.note = target.value;

                fetch('api/cleaning/update', defaultRequestOptions("POST", cle))
                .then((response) => {
                    console.log(response);
                    if (response.ok) {
                        updateList();
                    }
                    if (response.status == 400) {
                        message.error("Something went wrong. Please try again.")
                    }
                })
            }
        }
    }

    const cleanChange = (value, props) => {
        var room;
        for (let obj of cleaning) {
            if (obj.reservation.room.id == props.name) {
                room = obj.reservation.room;
            }
        }
        if (room) {
            room.clean = value;
            fetch('api/rooms/update', defaultRequestOptions("POST", room))
            .then((response) => {
                console.log(response);
                if (response.ok) {
                    updateList();
                }
                if (response.status == 400) {
                    message.error("Something went wrong. Please try again.")
                }
            })
        }
    }

    return(
        <>
            <Table dataSource={dataSource} columns={columns} />
        </>
    )
}