import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, message, Space } from 'antd';
import React from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const options = [
    {
      label: 'Balcony',
      value: 'balcony',
    },
    {
      label: 'Kitchenette',
      value: 'kitchenette',
    },
    {
      label: 'Sea View',
      value: 'seaView',
    },
    {
        label: 'Hot tub',
        value: 'hotTub',
    },
];

const UpdateRoomCategories = ({formValues}) => {

    const onFinish = (values) => {
        var list = [];

        for (let value of values.categories) {
            var object = {};

            object.id = value.id;
            object.name = value.name;
            object.size = value.size;
            object.price = value.price;
            object.balcony = false;
            object.hotTub = false;
            object.seaView = false;
            object.kitchenette = false;

            for (let booleanValue of value.booleanValues) {
                if (booleanValue == 'balcony')
                    object.balcony = true;
                if (booleanValue == 'hotTub')
                    object.hotTub = true;
                if (booleanValue == 'seaView')
                    object.seaView = true;
                if (booleanValue == 'kitchenette')
                    object.kitchenette = true;
            }
            list.push(object);
        }

        UpdateRoomCategories(list);
    };

    const UpdateRoomCategories = (list) => {
        for (var value of list) {
            try {
                fetch('api/categories', defaultRequestOptions("POST", value))
                .then((response) => {
                    if (response.ok) {
                        message.success('Changes are saved successfuly.');
                    }
                    else {
                        message.error('Something went wrong. Please try again.')
                    }
                })
            } catch (err) {
                message.error('Cannot make connection to server. Please try again.')
            }
        }
    }

  return (
    <>
    <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
        <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
            <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Room categories</div>
        </Space>
    </div>
    <Form name="dynamic_form_nest_item"
        onFinish={onFinish}
        autoComplete="off"
        initialValues={{ categories: formValues }}>
      <Form.List name="categories">
        {(fields, { add, remove }) => (
          <>
            {fields.map(({ key, name, ...restField }) => (
              <Space
                key={key}
                style={{
                  display: 'flex',
                  marginBottom: 8,
                }}
                align="baseline"
              >
                <Form.Item
                  {...restField}
                  name={[name, 'name']}
                  rules={[
                    {
                      required: true,
                      message: 'Name cannot be empty.',
                    },
                  ]}
                >
                  <Input placeholder="Name" />
                </Form.Item>
                <Form.Item
                  {...restField}
                  name={[name, 'id']}
                >
                  <Input type={'hidden'}/>
                </Form.Item>
                <Form.Item
                  {...restField}
                  name={[name, 'size']}
                  rules={[
                    {
                      required: true,
                      message: 'Size cannot be empty.',
                    },
                  ]}
                >
                  <Input placeholder="Size" />
                </Form.Item>
                <Form.Item
                  {...restField}
                  name={[name, 'price']}
                  rules={[
                    {
                      required: true,
                      message: 'Price cannot be empty.',
                    },
                  ]}
                >
                  <Input placeholder="Price" />
                </Form.Item>
                <Form.Item
                  {...restField}
                  name={[name, 'booleanValues']}
                >
                  <Checkbox.Group options={options} />
                </Form.Item>
                <MinusCircleOutlined onClick={() => remove(name)} />
              </Space>
            ))}
            <Form.Item>
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                Add category
              </Button>
            </Form.Item>
          </>
        )}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
    </>
  );
};

export default UpdateRoomCategories;