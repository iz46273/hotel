import { Progress, Statistic, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { Text } = Typography;

export default function ReceptionStatistics() {

    const [statistics, setStatistics] = useState({});

    useEffect(() => {
        fetchStatistics();   
    }, [])

    const fetchStatistics = () => {
        fetch('/api/statistics/reception', defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            console.log(list);
            setStatistics(list);
        });
    }

    return(
        <>
            <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                <Text type="secondary">Occupancy</Text><br></br><br></br>
                <Progress type="dashboard" percent={statistics.occupied} size="small" />
            </div>
            <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                <Text type="secondary">To arrive</Text><br></br><br></br>
                <Progress
                    type="dashboard"
                    percent={statistics.arrivalPercentage}
                    size="small"
                    format={() => `${statistics.toArrive}`}
                    />
            </div>
            <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                <Text type="secondary">Due out</Text><br></br><br></br>
                <Progress
                    type="dashboard"
                    percent={statistics.checkedOutPercentage}
                    size="small"
                    format={() => `${statistics.dueOut}`}
                    />
            </div>
            <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '50px', textAlign: 'center'}}>
                <Statistic title="Available rooms" value={statistics.availableRooms} />
            </div>
            <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '50px', textAlign: 'center'}}>
                <Statistic title="Guests in hotel" value={statistics.guestsInHotel} />
            </div>
            <div style={{float: 'left', padding: '30px', height: '150px', width: '550px'}}>
                <Text type="secondary" style={{float: 'left'}}>Departured</Text>
                <Progress percent={statistics.checkedOutPercentage} style={{float: 'right', width: '380px'}} /><br></br>
                <Text type="secondary" style={{float: 'left'}}>Arrived</Text>
                <Progress percent={statistics.arrivalPercentage} style={{float: 'right', width: '380px'}} /><br></br>
                <Text type="secondary" style={{float: 'left'}}>Cleaned</Text>
                <Progress percent={statistics.cleanedRooms / statistics.allRoomsToClean * 100} style={{float: 'right', width: '380px'}} /><br></br>
            </div>
        </>
    )
}