import { Table } from 'antd';
import React, { useEffect, useState } from 'react';

export default function AllGuestsTable({ guests, refresh, setSelectedGuest, setPage }) {

    const [dataSource, setDataSource] = useState();

    const columns = [
        {
          title: 'Id',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: 'First name',
          dataIndex: 'firstName',
          key: 'firstName',
        },
        {
            title: 'Last name',
            dataIndex: 'lastName',
            key: 'lastName',
        },
        {
            title: 'E-mail',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Country',
            dataIndex: 'country',
            key: 'country',
        },
        {
            title: 'Passport',
            dataIndex: 'passport',
            key: 'passport',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'VIP',
            dataIndex: 'vip',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                <>{record.vip.id}</>
            ) : null,
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) =>
              dataSource.length >= 1 ? (
                    <a onClick={() => handleUpdate(record.id)}>Edit</a>
            ) : null,
        },
    ];
      
    useEffect(() => {
        setDataSource(guests);
    }, [guests])

    const handleUpdate = (key) => {
        for (var g of guests) {
            if (g.id == key)
                setSelectedGuest(g);
        }
        setPage('update');
    };

    return (
        <>
            <Table dataSource={dataSource} columns={columns} size="small" />
        </>
    )
}