import React, { useState, useEffect, useRef } from 'react';
import RoomCard from './RoomCard'
import NewReservation from './NewReservation'
import { Button, Tooltip, DatePicker, Space, Radio, InputNumber, Spin } from 'antd';
import { TbUsers, TbCurrencyEuro } from 'react-icons/tb'
import { FiRefreshCcw } from 'react-icons/fi'
import { IoBed } from 'react-icons/io5'
import moment from 'moment';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { RangePicker } = DatePicker;

const range = (start, end) => {
    const result = [];
  
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    console.log(result);
    return result;
};
  
const disabledDate = (current) => {
    return current && current < moment().add(-1, 'day').endOf('day');
};

const doubleOptions = [
    {
        label: 'All',
        value: 'all',
      },
    {
      label: 'Double',
      value: 'double',
    },
    {
      label: 'Twin',
      value: 'twin',
    },
];

const cleanOptions = [
    {
        label: 'All',
        value: 'all',
      },
    {
      label: 'Clean',
      value: 'clean',
    },
    {
      label: 'Dirty',
      value: 'dirty',
    },
];

const sortOptions = [
    {
        label: <IoBed />,
        value: 'room',
      },
    {
      label: <TbCurrencyEuro />,
      value: 'price',
    },
    {
        label: <TbUsers />,
        value: 'capacity',
      },
];

export default function App({view}) {

    const [double, setDouble] = useState('all');
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [capacity, setCapacity] = useState(2);
    const [clean, setClean] = useState('all');
    const [sort, setSort] = useState('room');
    const [rooms, setRooms] = useState([]);
    const [dates, setDates] = useState(false);
    const [selectedRoom, setSelectedRoom] = useState();
    const isFirstRender = useRef(true);

    useEffect(() => {
        if (!isFirstRender.current) {
          filterRooms();
        }
    }, [double, startDate, endDate, capacity, clean])

    useEffect(() => {
        if (view == "allrooms")
            fetchRooms();
        let today = moment().startOf('day').format("YYYY-MM-DD");
        let tomorrow = moment().add(1, 'day').startOf('day').format("YYYY-MM-DD");
        if (view == "availableToday") {
            setStartDate(today);
            setEndDate(tomorrow);
            setDates(true);
        }
        if (view == "availableNow") {
            setStartDate(today);
            setEndDate(tomorrow);
            setClean("clean");
            setDates(true);
        }
        isFirstRender.current = false;
    }, [])

    const fetchRooms = () => {
        fetch('/api/rooms/open', defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            setRooms(roomList);
        });
    }

    const filterRooms = () => {
        fetch('/api/rooms/open/filter?' + new URLSearchParams({
            from: startDate,
            to: endDate,
            capacity : capacity,
            type: double,
            clean: clean
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            setRooms(roomList);
        });
    }

    const onCalendarChange = (value, dateString) => {
        if (dateString[0] == '' && dateString[1] == '') {
            setStartDate(dateString[0]);
            setEndDate(dateString[1]);
            setDates(false);
        }
        if (dateString[0] != '' && dateString[1] != '') {
            setStartDate(dateString[0]);
            setEndDate(dateString[1]);
            setDates(true);
        }
    }

    const onDoubleChange = ({ target: { value } }) => {
        setDouble(value);
    };

    const onCleanChange = ({ target: { value } }) => {
        setClean(value);
    };

    const onCapacityChange = (value) => {
        setCapacity(value);
    }

    const onSortChange = ({ target: { value } }) => {
        setSort(value);
        if (value == "room") {
            sortByRoom();
        }
        if (value == "price") {
            sortByPrice();
        }
        if (value == "capacity") {
            sortByCapacity();
        }
    };

    const sortByPrice = () => {
        let sortedAsceding = rooms.sort((a, b) => {
        return a.category.price - b.category.price;
        });
        setRooms(sortedAsceding);
    }

    const sortByRoom = () => {
        let sortedAsceding = rooms.sort((a, b) => {
        return a.id - b.id;
        });
        setRooms(sortedAsceding);
    }

    const sortByCapacity = () => {
        let sortedAsceding = rooms.sort((a, b) => {
        return a.capacity - b.capacity;
        });
        setRooms(sortedAsceding);
    }

    const handleSelectRoom = (roomValue) => {
        setSelectedRoom(roomValue);
    }

    return (
        <>
        { selectedRoom ?
            <NewReservation room={selectedRoom.room} startDate={startDate} endDate={endDate} />
            :
            <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                    {view == "allrooms" ?
                        <RangePicker disabledDate={disabledDate} onCalendarChange={onCalendarChange}/>
                    :
                        <RangePicker disabledDate={disabledDate} defaultValue={[moment(), moment().add(1, 'days')]} onCalendarChange={onCalendarChange}/>
                    }
                    <InputNumber addonBefore={<TbUsers />} style={{width: "100px"}} min={1} max={10} value={capacity} onChange={onCapacityChange} />
                    <Radio.Group options={doubleOptions} onChange={onDoubleChange} value={double} optionType="button" />
                    <Radio.Group options={cleanOptions} onChange={onCleanChange} value={clean} optionType="button" />
                    <a style={{color: "black"}}>Sort by:</a>
                    <Radio.Group options={sortOptions} onChange={onSortChange} value={sort} optionType="button" />
                </Space>
                <Tooltip title="Refresh">
                    <Button type="primary" onClick={filterRooms} shape="circle" size="large" icon={<FiRefreshCcw />} style={{float: 'right', margin: '5px', background: 'white', color: 'lightgrey'}}/>
                </Tooltip>
            </div>
            <div style={{overflow: 'auto', marginBottom:'500px', height:'85%', float: 'none'}}>
                {rooms ? 
                    rooms.length ?
                        rooms.map((room) => (
                            <RoomCard room={room} dates={dates} handleSelectRoom={handleSelectRoom}/>
                        ))
                    :  <div style={{textAlign: 'center'}}><br></br><br></br><br></br><Spin size="large" />&nbsp;&nbsp;&nbsp;Loading...</div>
                : <div style={{textAlign: 'center'}}><br></br><br></br><br></br>Error loading rooms.</div>}
            </div>
            </>
        }
        </>
    )
}