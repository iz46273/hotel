import { Divider, Space } from 'antd';
import React from 'react';
import HousekeepingStatistics from './HousekeepingStatistics';
import Messages from './Messages';

const HousekeepingDashboard = () => (
    <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'rgba(24,154,255,255)', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Housekeeping board</div>
            </Space>
        </div>
        <div style={{float: 'left', width: '55%', height: '85%'}}>
            <HousekeepingStatistics />
        </div> <Divider type="vertical" style={{ height: "85%" }} />
        <div style={{float: 'right', width: '40%', height: '85%', textAlign: 'center'}}>
            <Messages />
        </div>
    </>
);

export default HousekeepingDashboard;