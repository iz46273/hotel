import { Button, Card, message, Popconfirm } from 'antd';
import Flags from 'country-flag-icons/react/3x2';
import React from 'react';
import { BsCalendarMinus, BsCalendarPlus } from 'react-icons/bs';
import { GoNote } from 'react-icons/go';
import { MdOutlineCleaningServices } from 'react-icons/md';
import { TbUsers } from 'react-icons/tb';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export default function RoomCard({ reservation, view, refresh }) {

    const extra = () => {
        var Flag = Flags[reservation.guest.country];
        return(<div style={{marginRight: '20px'}}><Flag style={{height:'15px', width:'30px'}} />{reservation.guest.firstName} {reservation.guest.lastName}</div>)
    }

    const checkIn = () => {
        try {
            fetch('api/reservations/checkIn/' + reservation.id, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    refresh();
                    message.success('Checked in successfully.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const checkOut = () => {
        try {
            fetch('api/reservations/checkOut/' + reservation.id, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    refresh();
                    message.success('Checked out successfully.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const cancel = () => {
        try {
            fetch('api/reservations/cancel/' + reservation.id, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    refresh();
                    message.success('Reservation canceled successfully.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    return (
        <Card
            size="small"
            title={reservation.room.id}
            extra={extra()}
            style={{
            border: 'solid 1px lightskyblue',
            width: 300,
            float: 'left',
            margin: '10px',
            boxShadow: '5px 5px 5px #aaaaaa',
            fontFamily: 'Arial',
            fontWeight: 'light',
            }}
        >
            <table style={{width: '100%', textAlign: 'left', marginLeft: '20px', marginRight: '20px'}}>
                <tr>
                    <td style={{padding:'5px'}}><BsCalendarPlus size='15px'/> {reservation.checkInDate}</td>
                    <td style={{padding:'5px'}}><BsCalendarMinus size='15px'/> {reservation.checkOutDate}</td>
                </tr>
                <tr>
                    { reservation.room.clean ?
                    <td style={{padding:'5px'}}><MdOutlineCleaningServices size='15px'/> clean</td>
                    :
                    <td style={{padding:'5px'}}><MdOutlineCleaningServices size='15px'/> dirty</td>
                    }
                    <td style={{padding:'5px'}}><TbUsers /> {reservation.adults + reservation.children + reservation.babies}</td>
                </tr>
                <tr>
                    <td colSpan={2} style={{padding:'5px'}}><GoNote /> {reservation.receptionNote ? <> {reservation.receptionNote} </> : <>/</>}</td>
                </tr>
                <tr>
                    
                        { view == "arrival" ?
                            <>
                            <td>
                                <Button onClick={() => checkIn()}>Check in</Button>
                            </td>
                            <td>
                                <Popconfirm title="Sure to cancel?" onConfirm={() => cancel()}>
                                    <Button>Cancel</Button>
                                </Popconfirm>
                            </td>
                            </> : <></>
                        }
                        { view == "dueOut" ?
                            <td colSpan={2}>
                                <Button onClick={() => checkOut()}>Check out</Button>
                            </td> : <></>
                        }
                </tr>
            </table>
        </Card>
    )
}