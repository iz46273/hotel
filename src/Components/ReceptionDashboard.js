import { Divider, Space } from 'antd';
import React from 'react';
import Messages from './Messages';
import ReceptionStatistics from './ReceptionStatistics';

const ReceptionDashboard = () => (
    <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'rgba(24,154,255,255)', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Reception board</div>
            </Space>
        </div>
        <div style={{float: 'left', width: '55%', height: '85%'}}>
            <ReceptionStatistics />
        </div> <Divider type="vertical" style={{ height: "85%" }} />
        <div style={{float: 'right', width: '40%', height: '85%', textAlign: 'center'}}>
            <Messages />
        </div>
    </>
);

export default ReceptionDashboard;