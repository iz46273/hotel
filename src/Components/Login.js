import React, { useState } from 'react'; 
import { Button, Checkbox, Form, Input, message } from 'antd';
import Register from './Register';
import { FaHotel } from 'react-icons/fa'

export default function Login({ setCurrentUser }) {
    const [loadings, setLoadings] = useState([]);
    const [newAccount, setNewAccount] = useState(false);

    const onFinish = (values) => {
        fetch('api/auth/login', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify(values)
        })
        .then((response) => {
            if (response.ok) {
                response = response.json()
                .then((response) => {
                    console.log(response);
                    if (response.accessToken) {
                        localStorage.setItem("user", JSON.stringify(response));
                        setCurrentUser(response);
                        localStorage.setItem("token", response.accessToken);
                    }
                })
            }
            if (response.status == 404) {
                message.error("Employee with this username does not exist.")
            }
            if (response.status == 401) {
                message.error("Unauthorized.")
            }
        })
    };

    const enterLoading = (index) => {
        setLoadings((prevLoadings) => {
          const newLoadings = [...prevLoadings];
          newLoadings[index] = true;
          return newLoadings;
        });
        setTimeout(() => {
          setLoadings((prevLoadings) => {
            const newLoadings = [...prevLoadings];
            newLoadings[index] = false;
            return newLoadings;
          });
        }, 6000);
    };
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return(
        <div style={{backgroundColor: 'rgb(177, 225, 255)', height: '100vh', padding: '150px'}}>
        { newAccount ? 
            <Register setNewAccount={setNewAccount} />
        : <>
            <div style={{width: '500px', marginLeft: '300px', backgroundColor: 'white', border: '1px solid lightskyblue', borderRadius: '25px', boxShadow: '8px 8px 8px #aaaaaa', padding: '50px'}}>
                <div style={{padding: '10px', color:'white', fontSize: '120%', backgroundColor: 'rgba(24,154,255,255)', marginLeft: '50px', marginBottom: '30px', fontFamily: 'Courier New'}}>
                    h o t e l .
                </div>
            <Form
            name="form"
            requiredMark={false}
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            >
            <Form.Item
                label="Username"
                name="username"
                rules={[
                {
                    required: true,
                    message: 'Please input your username!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                {
                    required: true,
                    message: 'Please input your password!',
                },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                offset: 8,
                span: 16,
                }}
            >
                <Button type="primary" htmlType="submit" loading={loadings[0]} onClick={() => enterLoading(0)}>
                    Log in
                </Button>&nbsp;&nbsp;
                <Button type="default" onClick={() => setNewAccount(true)}>
                    New account
                </Button>
            </Form.Item>
            </Form>
            </div>
            </> }
        </div>
    )
}