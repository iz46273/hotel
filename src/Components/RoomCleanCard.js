import { Card } from 'antd';
import React from 'react';

export default function RoomCard ({reservation}) {

    return (
        <Card
            size="small"
            title={reservation.room.id}
            extra={reservation.room.category.name}
            style={{
            border: 'solid 1px lightskyblue',
            width: 300,
            float: 'left',
            margin: '10px',
            width: '200px',
            height: '80px',
            boxShadow: '5px 5px 5px #aaaaaa',
            fontFamily: 'Arial',
            fontWeight: 'light',
            }}
        >
            <div style={{width: '100%', textAlign: 'center'}}>
                { reservation.stayover ? <p> Stayover </p> : <></>}
                { reservation.departure ? <p> Departure </p> : <></>}
                { reservation.arrival ? <p> Arrival </p> : <></>}
            </div>
        </Card>
    )
}