import { Button, Checkbox, Divider, Space } from 'antd';
import { useEffect, useRef, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import MaidWorkingList from './MaidWorkingList';
import SetCleaning from './SetCleaning';

export default function App() {

    const isFirstRender = useRef(true);
    const [stayover, setStayover] = useState([]);
    const [departure, setDeparture] = useState([]);
    const [arrival, setArrival] = useState([]);
    const [dueOut, setDueOut] = useState([]);
    const [vacant, setVacant] = useState([]);
    const [allMaids, setAllMaids] = useState([]);
    const [maids, setMaids] = useState([]);
    const [allReservations, setAllReservations] = useState();
    const [page, setPage] = useState(1);
    const [reservationsLeft, setReservationsLeft] = useState([]);
    const [cleaningGenerated, setCleaningGenerated] = useState(false);

    useEffect(() => {
        fetchReservations();
        fetchMaids();
        checkIfGenerated();
        isFirstRender.current = false;
    }, [])

    useEffect(() => {
        setReservationsLeft(allReservations);
    }, [allReservations])

    useEffect(() => {
        var reservations = new Array();
        for (const reservation of stayover) {
            reservation.stayover = true;
            reservations.push(reservation);
        }
        for (const reservation of departure) {
            reservation.departure = true;
            reservations.push(reservation);
        }
        for (const reservation of dueOut) {
            reservation.dueOut = true;
            reservations.push(reservation);
        }
        for (const reservation of vacant) {
            reservation.vacant = true;
            reservations.push(reservation);
        }
        for (const i of arrival) {
            var check = true;
            for (const j of reservations) {
                if (i.room.id === j.room.id) {
                    j.arrival = true;
                    check = false;
                }
            }
            if (check) {
                i.arrival = true;
                reservations.push(i);
            }
        }

        let sorted = reservations.sort((a, b) => {
            return a.room.id - b.room.id;
        });

        let filtered = sorted.filter(res => res.room.clean == false);

        setAllReservations(filtered);
    }, [stayover, departure, arrival, dueOut, vacant])

    const fetchReservations = () => {
        fetch('/api/reservations/today/stayover', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setStayover(reservationList);
        });
        fetch('/api/reservations/today/departured', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setDeparture(reservationList);
        });
        fetch('/api/reservations/today/arrival', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setArrival(reservationList);
        });
        fetch('/api/reservations/today/dueOut', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            setDueOut(reservationList);
        });
        fetch('/api/reservations/today/vacant', defaultRequestOptions())
        .then((response) => response.json())
        .then(reservationList => {
            console.log(vacant);
            setVacant(reservationList);
        });
    }

    const checkIfGenerated = () => {
        fetch('/api/cleaning/checkIfGenerated', defaultRequestOptions())
        .then((response) => response.json())
        .then(check => {
            setCleaningGenerated(check);
        });
    }

    const fetchMaids = () => {
        fetch('/api/employees/getMaids', defaultRequestOptions())
        .then((response) => response.json())
        .then(maids => {
            setAllMaids(maids);
        });
    }

    const onCheckboxChange = (checkedValues) => {
        setMaids(checkedValues);
    };

    const submitPage1 = () => {
        setPage(2);
    }

    const removeReservations = (updateReservations) => {
        var reservations = reservationsLeft;
        var filtered = reservations.filter(res => res != updateReservations);
        setReservationsLeft(filtered);
    }

    const addReservations = (updateReservations) => {
        const reservations = [...reservationsLeft];
        reservations.push(updateReservations);

        let sorted = reservations.sort((a, b) => {
            return a.room.id - b.room.id;
        });

        setReservationsLeft(sorted);
    }

    const handleCleaningGenerated = (check) => {
        setCleaningGenerated(check);
    }

    return(
        <>
        { cleaningGenerated ? 
            <><MaidWorkingList /></>
            :
            <>
        { page == 1 ?
            <><div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', color:'white'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%'}}>Maids working today</div>
                </Space>
            </div><br></br>
            <div style={{overflow: 'auto', marginBottom:'500px', height:'85%', float: 'none'}}>
                <Checkbox.Group style={{ width: '100%' }} onChange={onCheckboxChange}>
                    { allMaids ? 
                        allMaids.length ?
                            allMaids.map((maid) => (
                                <><Checkbox value={maid}>{maid.firstName} {maid.lastName}</Checkbox><br></br></>
                            ))
                        :  <></>
                    : <></> }
                </Checkbox.Group>
                <Divider></Divider>
                <Button onClick={submitPage1}>
                    Submit
                </Button>
            </div>
            </>
            : <></>}
            { page == 2 ?
                <><SetCleaning maids={maids} reservationsLeft={reservationsLeft} removeReservations={removeReservations} addReservations={addReservations} handleCleaningGenerated={handleCleaningGenerated} /></>
            : <></>}
            </>
        }
        </>
    )
}