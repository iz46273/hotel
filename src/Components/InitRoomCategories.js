import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import UpdateRoomCategories from './UpdateRoomCategories';

const InitRoomCategories = () => {

    const [formValues, setFormValues] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        fetchCategory();
    }, [])

    useEffect(() => {
        if (formValues.length > 0)
            setShowForm(true);
    }, [formValues])

    useEffect(() => {
        var list = [];

        for (var category of categories) {
            let object = {};
            object.booleanValues = [];
            if (category.balcony)
                object.booleanValues.push('balcony');
            if (category.hotTub)
                object.booleanValues.push('hotTub');
            if (category.seaView)
                object.booleanValues.push('seaView');
            if (category.kitchenette)
                object.booleanValues.push('kitchenette');

            object.id = category.id;
            object.size = category.size;
            object.name = category.name;
            object.price = category.price;

            list.push(object);
        }
        setFormValues(list);
    }, [categories])

    const fetchCategory = () => {
        fetch('/api/categories', defaultRequestOptions())
        .then((response) => response.json())
        .then(catList => {
            setCategories(catList);
        });
    }

    return(
        <>
        {showForm ?
        <UpdateRoomCategories formValues={formValues} />
        : <></> }
        </>
    )
}

export default InitRoomCategories;