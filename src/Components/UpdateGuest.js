import {
    Button, Form,
    Input, message, Select, Space
} from 'antd';
import React, { useEffect, useState } from 'react';
import ReactFlagsSelect from "react-flags-select";
import PhoneInput from 'react-phone-number-input';

import { BsArrowLeft, BsArrowRight } from 'react-icons/bs';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

const { Option } = Select;

export default function UpdateGuest ({ selectedGuest, setPage }) {

    const [form] = Form.useForm();
    const [vipList, setVipList] = useState([]);
    const [country, setCountry] = useState(selectedGuest.country);

    useEffect(() => {
        fetchVip();
    }, [])

    const fetchVip = () => {
        fetch('/api/vip', defaultRequestOptions())
        .then((response) => response.json())
        .then(vipList => {
            setVipList(vipList);
        });
    }

    const withEmailCheck = (values) => {
        fetch('/api/guests/checkIfExists/' + values.email, defaultRequestOptions())
        .then((response) => response.json())
        .then(check => {
            if (check) {
                message.error("Guest with this email already exists!")
            }
            else {
                fetch('api/guests/update', defaultRequestOptions("POST", values))
                .then((response) => {
                    if (response.ok) {
                        message.success('Changes are saved successfuly.');
                        setPage('all');
                    }
                    else {
                        message.error('Something went wrong. Please try again.')
                    }
                })
            }
        });
    }

    const withoutEmailCheck = (values) => {
        fetch('api/guests/update', defaultRequestOptions("POST", values))
        .then((response) => {
            if (response.ok) {
                message.success('Changes are saved successfuly.');
                setPage('all');
            }
            else {
                message.error('Something went wrong. Please try again.')
            }
        })
    }

    const onFinish = (values) => {

        values.id = selectedGuest.id;
        var vipObject = vipList.find(x => x.id === values.vip);
        values.vip = vipObject;
        values.country = country;

        if (values.email != selectedGuest.email) {
            withEmailCheck(values);
        }
        else {
            withoutEmailCheck(values);
        }
    };

    return (
        <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                    <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Guest details: {selectedGuest.id}</div>
                </Space>
            </div>
        <Form
            {...formItemLayout}
            form={form}
            size="small"
            onFinish={onFinish}
            initialValues={{
                firstName: selectedGuest.firstName,
                lastName: selectedGuest.lastName,
                email: selectedGuest.email,
                passport: selectedGuest.passport,
                phone: selectedGuest.phone,
                vip: selectedGuest.vip.id,
            }}
            >
            <Form.Item name={["firstName"]} label="First name" rules={[
                {
                required: true,
                message: 'First name cannot be empty.',
                whitespace: true,
                },
            ]}>
                <Input />
            </Form.Item>

            <Form.Item name={["lastName"]} label="Last name" rules={[
                {
                required: true,
                message: 'Last name cannot be empty.',
                whitespace: true,
                },
            ]}>
                <Input />
            </Form.Item>

            <Form.Item name={["email"]} label="E-mail" rules={[
                {
                type: 'email',
                message: 'The input is not valid e-mail.',
                },
                {
                required: true,
                message: 'E-mail cannot be empty.',
                },
            ]}>
                <Input />
            </Form.Item>

            <Form.Item name={["passport"]} label="Passport" rules={[
                {
                required: true,
                message: 'Passport number cannot be empty.',
                },
            ]}>
                <Input />
            </Form.Item>

            <Form.Item label="Country" name={["guest", "country"]}>
                    <ReactFlagsSelect selected={country} onSelect={(code) => setCountry(code)} />
            </Form.Item>
    
            <Form.Item name={["phone"]} label="Phone Number">
                <PhoneInput placeholder="Enter phone number" international countryCallingCodeEditable={false} />
            </Form.Item>

            <Form.Item label="VIP" name={["vip"]}>
                <Select>
                    {vipList ? 
                        vipList.length ?
                            vipList.map((vip) => (
                                <Option value={vip.id}>{vip.id}</Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="default" onClick={() => setPage('all')}>
                        <BsArrowLeft />&nbsp;Go back
                </Button>&nbsp;&nbsp;
                <Button type="primary" htmlType="submit">
                    Submit changes&nbsp;<BsArrowRight />
                </Button>
            </Form.Item>
            </Form>
        </>
    )
}