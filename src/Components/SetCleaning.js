import { Button, Divider, message, Space } from 'antd';
import React, { useState } from "react";
import { MaidCheckboxGroup } from './MaidCheckboxGroup';

export default function SetCleaning({maids, reservationsLeft, removeReservations, addReservations, handleCleaningGenerated}) {
    const [save, setSave] = useState(false);
    const [loadings, setLoadings] = useState([]);
    
    const checkSubmit = () => {
        setLoadings((prevLoadings) => {
            const newLoadings = [...prevLoadings];
            newLoadings[0] = true;
            return newLoadings;
        });
        setTimeout(() => {
            setLoadings((prevLoadings) => {
              const newLoadings = [...prevLoadings];
              newLoadings[0] = false;
              return newLoadings;
            });
        }, 6000);
        if (reservationsLeft.length == 0) {
            setSave(true);
        }
        else {
            message.error("Every room must be assigned to a maid!");
        }
    }

  return (
    <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', color:'white'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%'}}>Assign rooms to maids</div>
                <div style={{float:'right', marginLeft:'150px', marginRight:'100px'}}>Maid number: {maids.length}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rooms to assign: {reservationsLeft.length}</div>
                <Button onClick={event => checkSubmit()} loading={loadings[0]} >Submit</Button>
            </Space>
        </div>
        <div style={{overflow: 'auto', marginBottom:'500px', height:'85%', float: 'none'}}>
        { maids ? 
                maids.length ?
                    maids.map((maid) => (
                        <div style={{border: '1px solid lightskyblue', float: 'left', borderRadius:'15px', padding:'10px', margin:'5px', height:'450px'}}>
                            {maid.firstName} {maid.lastName}
                            <Divider></Divider>
                            <MaidCheckboxGroup maid={maid} reservationsLeft={reservationsLeft} removeReservations={removeReservations} addReservations={addReservations} save={save} handleCleaningGenerated={handleCleaningGenerated} />
                        </div>
                    ))
                :  <></>
            : <></> }
        </div>
    </>
  );
}