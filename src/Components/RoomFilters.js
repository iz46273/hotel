import React, { Component } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export default class RoomFilters extends Component {

    state = {
        rooms: []
    }

    fetchRooms = () => {
        fetch('/api/rooms', defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            this.setState({ rooms: roomList });
        });
    }

    render(){
        return (
            <>
            <button onClick={this.fetchRooms}>Load</button>
                {this.state.rooms.map((room) => (
                <>
                    <Card
                      size="small"
                      title={room.id}
                      extra={<a href="#">More</a>}
                      style={{
                        width: 300,
                        float: 'left',
                        margin: '10px',
                        boxShadow: '10px 10px 5px #aaaaaa'
                      }}
                    >
                      <table style={{width: '85%', textAlign: 'center'}}>
                          <tr>
                              <td><TbUsers /> {room.capacity}</td>
                              <td>{room.category}</td>
                          </tr>
                          <tr>
                              <td>{room.clean}</td>
                              <td>{room.type}</td>
                          </tr>
                      </table>
                    </Card>
                </>
                ))}
            </>
        )
    }

    componentDidMount() {
        fetch('/api/rooms', defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            this.setState({ rooms: roomList });
        });
    }
}