import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import UpdatePansions from './UpdatePansions';

const InitPansions = () => {

    const [showForm, setShowForm] = useState(false);
    const [pansions, setPansions] = useState([]);

    useEffect(() => {
        fetchPansions();
    }, [])

    useEffect(() => {
        if (pansions.length > 0)
            setShowForm(true);
    }, [pansions])

    const fetchPansions = () => {
        fetch('/api/pansions', defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            setPansions(list);
        });
    }

    return(
        <>
        {showForm ?
        <UpdatePansions formValues={pansions} />
        : <></> }
        </>
    )
}

export default InitPansions;