import { Select, message, Button, Space, Alert } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import 'react-phone-number-input/style.css';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { Option } = Select;

export default function EditReservationMoveToAnotherRoom({reservation, setPage}) {

    const [room, setRoom] = useState();
    const [rooms, setRooms] = useState();

    useEffect(() => {
        fetchAvailableRooms();
    }, [])

    const fetchAvailableRooms = () => {
        fetch('/api/rooms/open/filter?' + new URLSearchParams({
            from: reservation.checkInDate,
            to: reservation.checkOutDate,
            capacity : 1,
            type: "all",
            clean: "all"
        }), defaultRequestOptions())
        .then((response) => response.json())
        .then(roomList => {
            console.log(roomList);
            setRooms(roomList);
        });
    }

    const submit = () => {
        if (room) {
            var today = moment().startOf('day');
            var checkIn = moment(reservation.checkInDate);
            var checkOut = moment(reservation.checkOutDate);

            if (checkIn >= today) {
                changeRoom();
            }
            else if (checkOut > today) {
                shortenOriginalReservation();
                makeNewReservation();
            }
            else
                message.warning("It is no logner possible to change room.")
        }
        else
            message.error("Please select room.")
        
    }

    const shortenOriginalReservation = () => {
        var today = moment().format('YYYY-MM-DD');
        try {
            fetch('/api/reservations/updateCheckOutDate/' + reservation.id + '/' + today, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    fetch('api/reservations/checkOut/' + reservation.id, defaultRequestOptions())
                    .then((response) => {
                        if (response.ok) {
                            message.success('Checked out from the previous room successfully.');
                        }
                        else {
                            message.error('Something went wrong. Please try again.')
                        }
                    }) 
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const makeNewReservation = () => {
        var today = moment().format('YYYY-MM-DD');
        reservation.checkInDate = today;
        var roomObject = rooms.find(x => x.id === room);
        reservation.room = roomObject;
        console.log(reservation);
        try {
            fetch('api/reservations/newAndCheckIn', defaultRequestOptions("POST", reservation))
            .then((response) => {
                if (response.ok) {
                    message.success('Checked in the new room successfully.');
                    setPage('all');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const changeRoom = () => {
        try {
            fetch('/api/reservations/changeRoom/' + reservation.id + '/' + reservation.room.id, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    message.success('Changes are saved successfuly.');
                    setPage('all');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const handleChange = (value) => {
        console.log(`selected ${value}`);
        setRoom(value);
    };

    return (
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', color: 'white', fontSize:'150%'}}>
                    Move guests to another room
                </Space>
            </div>
            <br></br>
            <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px', marginRight: '100px'}}>
                <Alert
                    message="Choose another room"
                    description="Choose another room where the guests will move.
                    The list contains rooms available for their selected check-in and check-out dates.
                    If the guests already have spent at least 1 night in their original room, the reservation will be split in 2 reservations.
                    Changes are possible before check-out date."
                    type="info"
                    /><br></br>
                <table>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Check-in date</td>
                        <td style={{padding: '10px'}}>{reservation.checkInDate}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Check-out date</td>
                        <td style={{padding: '10px'}}>{reservation.checkOutDate}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Old room</td>
                        <td style={{padding: '10px'}}>{reservation.room.id}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>Room category</td>
                        <td style={{padding: '10px'}}>{reservation.room.category.name}</td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>New room</td>
                        <td style={{padding: '10px'}}>
                        <Select
                            style={{
                                width: 180,
                            }}
                            onChange={handleChange}
                            value={room}
                            >
                            {rooms ? 
                                rooms.length ?
                                    rooms.map((r) => (
                                        <Option value={r.id}>{r.id} {r.category.name}</Option>
                                    ))
                                :  <></>
                            : <></>}
                            
                        </Select>
                        </td>
                    </tr>
                    <tr>
                        <td style={{padding: '10px', textAlign: 'right'}}>
                        <Button type="default" onClick={() => setPage('all')}>
                            Go back
                        </Button>
                        </td>
                        <td style={{padding: '10px'}}>
                        <Button type="primary" onClick={() => submit()}>
                            Submit changes
                        </Button>
                        </td>
                    </tr>
                </table>
            </div>
        </>
    )
}