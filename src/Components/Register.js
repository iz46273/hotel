import React, { useState } from 'react'; 
import { Button, Checkbox, Form, Input, message } from 'antd';

export default function Register({setNewAccount}) {

    const onFinish = (values) => {
        if (values.password == values.confirmPassword) {
            fetch('api/auth/register', {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify(values)
            })
            .then((response) => {
                if (response.ok) {
                    message.success("Your password is successfully set.");
                    setNewAccount(false);
                }
                if (response.status == 404) {
                    message.error("Employee with this username does not exist.");
                }
                if (response.status == 401) {
                    message.error("Password is already set.");
                }
            })
        }
        else {
            message.warning("Passwords do not match.");
        }
    };

    return(
        <div style={{width: '500px', marginLeft: '300px', backgroundColor: 'white', border: '1px solid lightskyblue', borderRadius: '25px', boxShadow: '8px 8px 8px #aaaaaa', padding: '50px'}}>
            <div style={{padding: '10px', color:'white', fontSize: '120%', backgroundColor: 'rgba(24,154,255,255)', marginLeft: '50px', marginBottom: '30px', fontFamily: 'Courier New'}}>
                h o t e l .
            </div>
            <h3>Set password</h3><br></br>
            <Form
            name="form"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            autoComplete="off"
            >
            <Form.Item
                label="Username"
                name="username"
                rules={[
                {
                    required: true,
                    message: 'Please input your username!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                {
                    required: true,
                    message: 'Please input your password!',
                },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                label="Confirm password"
                name="confirmPassword"
                rules={[
                {
                    required: true,
                    message: 'Please input your password!',
                },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                offset: 8,
                span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>&nbsp;&nbsp;
                <Button type="default" onClick={() => setNewAccount(false)}>
                    Go back
                </Button>
            </Form.Item>
            </Form>
            </div>
    );
}