import { Button, message } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { BsCheck2 } from 'react-icons/bs';
import { RiArrowGoBackLine } from 'react-icons/ri';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import './ReservationDetails.css';

export default function App({values, newGuest, goBack}) {

    const [stayBasePrice, setStayBasePrice] = useState();
    const [nightNumber, setNightNumber] = useState(0);
    const [totalPrice, setTotalPrice] = useState();
    const [adultRoomPrice, setAdultRoomPrice] = useState();
    const [childRoomPrice, setChildRoomPrice] = useState();

    useEffect(() => {
        fetchStayBasePrice();
        calculateNightNumber();
    }, [])

    useEffect(() => {
        var adultRoomPrice = stayBasePrice * 0.15;
        var childRoomPrice = stayBasePrice * 0.15 / 2;

        setAdultRoomPrice(adultRoomPrice);
        setChildRoomPrice(childRoomPrice);

        var adultPrice = values.pansion.price + adultRoomPrice;
        var childPrice = (values.pansion.price / 2) + childRoomPrice;

        var price = stayBasePrice + (adultPrice * values.adults) + (childPrice * values.children);
        values.totalPrice = price;
    }, [stayBasePrice])

    const calculateNightNumber = () => {
        const start = moment(values.checkInDate, 'YYYY-MM-DD');
        const end = moment(values.checkOutDate, 'YYYY-MM-DD');
        let diff = end.diff(start, 'days');
        setNightNumber(diff);
    }

    const fetchStayBasePrice = () => {
        fetch('/api/rooms/getStayPrice?roomId=' + values.room.id + '&from=' + values.checkInDate + '&to=' + values.checkOutDate, defaultRequestOptions())
            .then((response) => response.json())
            .then(price => {
                setStayBasePrice(price);
            }
        );
    }

    const submit = () => {
        try {
            console.log("uslo");
            fetch("/api/reservations/new", defaultRequestOptions("POST", values))
            .then((response) => {
                if (response.ok) {
                    message.success('Reservation is made successfully.');
                    goBack(3);
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    return(
        <>
        <div style={{float: 'none', width: '75%', height:'40px', background: 'lightskyblue', marginBottom: '15px', marginLeft: '100px', textAlign: 'center'}}>
            <h2 style={{color: 'white', marginLeft:'20px'}}>Reservation summary</h2>
        </div>
        <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px'}}>
            <table class="myTable">
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd">Check-in date</td>
                    <td class="MyTd">{values.checkInDate}</td>
                </tr>
                <tr class="MyTr">
                <td class="MyTd"></td>
                    <td class="MyTd">Check-out date</td>
                    <td class="MyTd">{values.checkOutDate}</td>
                </tr>
                <tr className="MyTr">
                    <td class="MyTd" rowSpan={2} style={{backgroundColor:'rgba(230,247,255,255)'}}>Room</td>
                    <td class="MyTd">Room number</td>
                    <td class="MyTd">{values.room.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Category</td>
                    <td class="MyTd">{values.room.category.name}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowspan={7}  style={{backgroundColor:'rgba(230,247,255,255)'}}>Guest</td>
                    <td class="MyTd">First Name</td>
                    <td class="MyTd">{values.guest.firstName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Last Name</td>
                    <td class="MyTd">{values.guest.lastName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Email</td>
                    <td class="MyTd">{values.guest.email}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Country</td>
                    <td class="MyTd">{values.guest.country}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Passport number</td>
                    <td class="MyTd">{values.guest.passport}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">Phone number</td>
                    <td class="MyTd">{values.guest.phone}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">VIP status</td>
                    <td class="MyTd">{values.guest.vip.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd">Receptionist</td>
                    <td class="MyTd">{values.receptionist.firstName} {values.receptionist.lastName}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowspan={4}  style={{backgroundColor:'rgba(230,247,255,255)'}}>Meal plan</td>
                    <td class="MyTd">Name</td>
                    <td class="MyTd">{values.pansion.id}</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">per adult price</td>
                    <td class="MyTd">{values.pansion.price} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">per child price</td>
                    <td class="MyTd">{values.pansion.price / 2} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>Total meal plan price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>{(values.pansion.price * values.adults) + ((values.pansion.price / 2) * values.children)} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" rowSpan={4} style={{backgroundColor:'rgba(230,247,255,255)'}}>Room price</td>
                    <td class="MyTd">Basic price</td>
                    <td class="MyTd">{stayBasePrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">+ per adult price</td>
                    <td class="MyTd">{adultRoomPrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd">+ per child price</td>
                    <td class="MyTd">{childRoomPrice} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>Total room price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}>{stayBasePrice + (adultRoomPrice * values.adults) + (childRoomPrice * values.children)} €</td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)', fontSize: 'large'}}>Total price</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)', fontSize: 'large'}}>{values.totalPrice} €</td>
                    <td class="MyTd" style={{backgroundColor:'rgba(230,247,255,255)'}}></td>
                </tr>
                <tr class="MyTr">
                    <td class="MyTd"></td>
                    <td class="MyTd"><Button onClick={event => goBack(1)} style={{borderRadius: "5px"}}><RiArrowGoBackLine />&nbsp;Back</Button></td>
                    <td class="MyTd"><Button onClick={submit} style={{borderRadius: "5px"}}><BsCheck2 />&nbsp;Submit</Button></td>
                </tr>
            </table>
        </div>
        </>
    )
}