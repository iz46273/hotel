import {
    Button,
    Form,
    Input, Select, Space
} from 'antd';
import moment from 'moment';
import React, { useState } from 'react';
import { BsArrowRight } from 'react-icons/bs';

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

const { Option } = Select;

export default function NewEmployee () {

    const [form] = Form.useForm();
    const [hasAccount, setHasAccount] = useState(false);

    const onFinish = (values) => {
        var today = moment().startOf('day').format("YYYY-MM-DD");
        values.accountCreated = today;

        if (hasAccount) {
            const arr = values.email.split("@");
            values.username = arr[0];
        }

        console.log(values);
        //TODO
        
        /*try {
            fetch('api/employees', defaultRequestOptions("POST", value))
            .then((response) => {
                if (response.ok) {
                    message.success('Changes are saved successfuly.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
        }*/
    };

    const handlePositionChange = (value) => {
        if (value == "MAID")
            setHasAccount(false);
        else
            setHasAccount(true);
    }

    return (
        <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>New employee</div>
            </Space>
        </div>
        <Form
            {...formItemLayout}
            form={form}
            size="small"
            onFinish={onFinish}
            initialValues={{
                active: true,
            }}
            >
            <Form.Item label="First name" name="firstName" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item label="Last name" name="lastName" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item label="Position" name="position" rules={[{required: true}]}>
                <Select onChange={(value) => handlePositionChange(value)}>
                    <Select.Option value="ADMINISTRATION">Administration</Select.Option>
                    <Select.Option value="HOUSEKEEPING">Housekeeping</Select.Option>
                    <Select.Option value="MAID">Maid</Select.Option>
                    <Select.Option value="RECEPTION">Reception</Select.Option>
                </Select>
            </Form.Item>
            { hasAccount ?
                <>
                    <Form.Item label="Email" name="email" rules={[
                            {
                            type: 'email',
                            message: 'The input is not valid e-mail.',
                            },
                            {
                            required: true,
                            message: 'E-mail cannot be empty.',
                            },
                        ]}>
                        <Input />
                    </Form.Item>
                </>
            : <></>}
            <Form.Item label="Active" name="active" rules={[{required: true}]}>
                <Select>
                    <Select.Option value={true}>Active</Select.Option>
                    <Select.Option value={false}>Inactive</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item label="Phone number" name="phone" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit changes&nbsp;<BsArrowRight />
                </Button>
            </Form.Item>
            </Form>
        </>
    )
}