import { PlusOutlined } from '@ant-design/icons';
import {
    Button, Form,
    Input,
    InputNumber, message, Select, Space, Tag,
    Tooltip
} from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { BsArrowRight } from 'react-icons/bs';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

const { Option } = Select;

export default function NewRoom () {

    const [form] = Form.useForm();
    const [categories, setCategories] = useState([]);
    const [types, setTypes] = useState([]);

    const [tags, setTags] = useState([]);
  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [editInputIndex, setEditInputIndex] = useState(-1);
  const [editInputValue, setEditInputValue] = useState('');
  const inputRef = useRef(null);
  const editInputRef = useRef(null);

    useEffect(() => {
        fetchCategory();
        fetchType();
    }, [])

    const fetchCategory = () => {
        fetch('/api/categories', defaultRequestOptions())
        .then((response) => response.json())
        .then(catList => {
            setCategories(catList);
        });
    }

    const fetchType = () => {
        fetch('/api/roomTypes', defaultRequestOptions())
        .then((response) => response.json())
        .then(typeList => {
            setTypes(typeList);
        });
    }

    useEffect(() => {
        if (inputVisible) {
          inputRef.current?.focus();
        }
      }, [inputVisible]);
      useEffect(() => {
        editInputRef.current?.focus();
    }, [inputValue]);
    
    const handleClose = (removedTag) => {
        const newTags = tags.filter((tag) => tag !== removedTag);
        setTags(newTags);
    };
    
    const showInput = () => {
        setInputVisible(true);
    };
    
    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    };
    
    const handleInputConfirm = () => {
        if (inputValue && tags.indexOf(inputValue) === -1) {
          setTags([...tags, inputValue]);
        }
    
        setInputVisible(false);
        setInputValue('');
    };
    
    const handleEditInputChange = (e) => {
        setEditInputValue(e.target.value);
    };
    
    const handleEditInputConfirm = () => {
        const newTags = [...tags];
        newTags[editInputIndex] = editInputValue;
        setTags(newTags);
        setEditInputIndex(-1);
        setInputValue('');
    };

    const onFinish = (values) => {
        if (tags.length < 1) {
            message.warning("There must be at least 1 room.")
        }
        else {
            for (var roomId of tags) {
                var value = {};

                value.id = Number(roomId);
                value.clean = true;
                value.capacity = values.capacity;
                value.open = values.open;

                var cat = categories.find(x => x.id === values.category);
                value.category = cat;

                var type = types.find(x => x.name === values.type);
                value.type = type;

                try {
                    fetch('api/rooms', defaultRequestOptions("POST", value))
                    .then((response) => {
                        if (response.ok) {
                            message.success('Changes are saved successfuly.');
                        }
                        else {
                            message.error('Something went wrong. Please try again.')
                        }
                    })
                } catch (err) {
                    message.error('Cannot make connection to server. Please try again.')
                }
            }
        }
    };

    return (
        <>
        <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
            <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px'}}>
                <div style={{float:'left', fontWeight: 'bold', fontSize: '150%', color:'white'}}>Add multiple rooms</div>
            </Space>
        </div>
        {tags.map((tag, index) => {
            if (editInputIndex === index) {
            return (
                <Input
                ref={editInputRef}
                key={tag}
                size="small"
                className="tag-input"
                value={editInputValue}
                onChange={handleEditInputChange}
                onBlur={handleEditInputConfirm}
                onPressEnter={handleEditInputConfirm}
                />
            );
            }

            const isLongTag = tag.length > 20;
            const tagElem = (
            <Tag
                className="edit-tag"
                key={tag}
                closable={index}
                onClose={() => handleClose(tag)}
            >
                <span
                onDoubleClick={(e) => {
                    if (index !== 0) {
                    setEditInputIndex(index);
                    setEditInputValue(tag);
                    e.preventDefault();
                    }
                }}
                >
                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                </span>
            </Tag>
            );
            return isLongTag ? (
            <Tooltip title={tag} key={tag}>
                {tagElem}
            </Tooltip>
            ) : (
            tagElem
            );
        })}
        {inputVisible && (
            <Input
            ref={inputRef}
            type="text"
            size="small"
            className="tag-input"
            value={inputValue}
            onChange={handleInputChange}
            onBlur={handleInputConfirm}
            onPressEnter={handleInputConfirm}
            />
        )}
        {!inputVisible && (
            <Tag className="site-tag-plus" onClick={showInput}>
            <PlusOutlined /> Add room
            </Tag>
        )}
        <Form
            {...formItemLayout}
            form={form}
            size="small"
            onFinish={onFinish}
            initialValues={{
                capacity: 2,
                category: 2,
                type: "double",
                open: true,
            }}
            >
            <Form.Item label="Capacity" name="capacity" rules={[{required: true}]}>
                <InputNumber />
            </Form.Item>
            <Form.Item label="Category" name="category" rules={[{required: true}]}>
                <Select>
                    {categories ? 
                        categories.length ?
                            categories.map((categ) => (
                                <Option value={categ.id}>{categ.name}</Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
                </Form.Item>
            <Form.Item label="Open" name="open" rules={[{required: true}]}>
                <Select>
                    <Select.Option value={true}>Open</Select.Option>
                    <Select.Option value={false}>Closed</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item label="Type" name="type" rules={[{required: true}]}>
                <Select>
                    {types ? 
                        types.length ?
                            types.map((type) => (
                                <Option value={type.name}>{type.name}</Option>
                            ))
                        :  <></>
                    : <></>}
                </Select>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit changes&nbsp;<BsArrowRight />
                </Button>
            </Form.Item>
            </Form>
        </>
    )
}