import { Empty, Progress, Statistic, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { Text } = Typography;

export default function HousekeepingStatistics() {

    const [statistics, setStatistics] = useState({});
    const [cleaningGenerated, setCleaningGenerated] = useState(false);

    useEffect(() => {
        fetchStatistics();   
        checkIfGenerated();  
    }, [])

    const fetchStatistics = () => {
        fetch('/api/statistics/housekeeping', defaultRequestOptions())
        .then((response) => response.json())
        .then(stat => {
            console.log(stat);
            if (stat.allRoomsToClean > 0) {
                stat.cleanedPercentage = Math.round(stat.cleanedRooms / stat.allRoomsToClean * 100);
            }
            else
                stat.cleanedPercentage = 100;
            setStatistics(stat);
        });
    }

    const checkIfGenerated = () => {
        fetch('/api/cleaning/checkIfGenerated', defaultRequestOptions())
        .then((response) => response.json())
        .then(check => {
            setCleaningGenerated(check);
        });
    }

    return(
        <>
            {
                cleaningGenerated ?
            <>
                <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                    <Text type="secondary">Maids working today</Text><br></br><br></br>
                    <Progress
                        type="dashboard"
                        percent={statistics.maidsWorkingToday / statistics.allMaids * 100}
                        size="small"
                        format={() => `${statistics.maidsWorkingToday}/${statistics.allMaids}`}
                        />
                </div>
                <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                    <Text type="secondary">Rooms to clean</Text><br></br><br></br>
                    <Progress
                        type="dashboard"
                        percent={statistics.cleanedPercentage}
                        size="small"
                        format={() => `${statistics.allRoomsToClean - statistics.cleanedRooms}`}
                        />
                </div>
                <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '30px', textAlign: 'center'}}>
                    <Statistic title="Rooms per maid" value={statistics.averageRoomsPerMaid} precision={1} />
                </div>
                <div style={{float: 'left', padding: '30px', height: '150px', width: '550px', marginTop:'100px'}}>
                    <Text type="secondary" style={{float: 'left'}}>Departured</Text>
                    <Progress percent={statistics.checkedOutPercentage} style={{float: 'right', width: '380px'}} /><br></br>
                    <Text type="secondary" style={{float: 'left'}}>Arrived</Text>
                    <Progress percent={statistics.arrivalPercentage} style={{float: 'right', width: '380px'}} /><br></br>
                    <Text type="secondary" style={{float: 'left'}}>Cleaned</Text>
                    <Progress percent={statistics.cleanedPercentage} style={{float: 'right', width: '380px'}} /><br></br>
                </div>
                
            </>
            :
                <div>
                    <Empty
                        image={Empty.PRESENTED_IMAGE_SIMPLE}
                        description={
                            <span style={{fontSize: 'larger'}}>
                                Please create maids' worksheets for today.
                            </span>
                            } 
                        />
                </div>
            }
        </>
    )
}