import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { Progress, Statistic, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { Text } = Typography;

export default function AdminStatistics() {

    const [statistics, setStatistics] = useState({});

    useEffect(() => {
        fetchStatistics();     
    }, [])

    const fetchStatistics = () => {
        fetch('/api/statistics/admin', defaultRequestOptions())
        .then((response) => response.json())
        .then(list => {
            console.log(list);
            setStatistics(list);
        });
    }

    return(
        <>
            <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                <Text type="secondary">Today's room occupancy</Text><br></br><br></br>
                <Progress type="dashboard" percent={statistics.currentOccupancy} size="small" />
            </div>
            <div style={{float: 'left', padding: '30px', height: '150px', textAlign: 'center'}}>
                <Text type="secondary">Average this month</Text><br></br><br></br>
                <Progress type="dashboard" percent={statistics.monthlyOccupancy} size="small" />
            </div>
            <div style={{float: 'left', padding: '30px', height: '150px'}}>
            {
                statistics.monthlyOccupancyDifference >= 0 ?
                    
                    <Statistic
                    title="Last month occupancy change"
                    value={statistics.monthlyOccupancyDifference}
                    precision={2}
                    valueStyle={{
                    color: '#3f8600',
                    }}
                    prefix={<ArrowUpOutlined />}
                    suffix="%"
                    />
                :
                    <Statistic
                    title="Last month occupancy change"
                    value={statistics.monthlyOccupancyDifference}
                    precision={2}
                    valueStyle={{
                    color: '#cf1322',
                    }}
                    prefix={<ArrowDownOutlined />}
                    suffix="%"
                    />
                    
            }
            </div>
            <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '30px'}}>
                <Statistic title="Monthly earnings" value={statistics.monthlyEarnings} suffix={"€"} precision={2} />
            </div>
            <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '30px'}}>
            {
                statistics.monthlyEarningsDifference >= 0 ?
                    
                    <Statistic
                    title="Last month earning change"
                    value={statistics.monthlyEarningsDifference}
                    precision={2}
                    valueStyle={{
                    color: '#3f8600',
                    }}
                    prefix={<ArrowUpOutlined />}
                    suffix="%"
                    />
                :
                    <Statistic
                    title="Last month earning change"
                    value={statistics.monthlyEarningsDifference}
                    precision={2}
                    valueStyle={{
                    color: '#cf1322',
                    }}
                    prefix={<ArrowDownOutlined />}
                    suffix="%"
                    />
            }
            </div>
            <div style={{float: 'left', padding: '30px', height: '100px', marginTop: '30px'}}>
                <Statistic title="Average nights per stay" value={statistics.averageNightsPerStay} precision={2} />
            </div>
            <div style={{float: 'left', padding: '50px', height: '100px'}}>
                <Statistic title="Yearly earnings" value={statistics.yearlyEarnings} suffix={"€"} precision={2} />
            </div>
            <div style={{float: 'left', padding: '50px', height: '100px'}}>
            {
                statistics.yearlyEarningsDifference >= 0 ?
                    
                    <Statistic
                    title="Last year change"
                    value={statistics.yearlyEarningsDifference}
                    precision={2}
                    valueStyle={{
                    color: '#3f8600',
                    }}
                    prefix={<ArrowUpOutlined />}
                    suffix="%"
                    />
                :
                    <Statistic
                    title="Last year change"
                    value={statistics.yearlyEarningsDifference}
                    precision={2}
                    valueStyle={{
                    color: '#cf1322',
                    }}
                    prefix={<ArrowDownOutlined />}
                    suffix="%"
                    />
                    
            }
            </div>
        </>
    )
}