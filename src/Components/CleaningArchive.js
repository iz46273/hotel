import { DatePicker, Empty, Tabs } from 'antd';
import moment from 'moment';
import { useEffect, useRef, useState } from 'react';
import defaultRequestOptions from '../helpers/defaultRequestOptions';
import MaidWorkingListPdf from './MaidWorkingListPdf';

const { TabPane } = Tabs;

const disabledDate = (current) => {
    return current && current > moment().add(-1, 'day');
  };

export function CleaningArchive() {
    const isFirstRender = useRef(true);
    const [ allCleaning, setAllCleaning ] = useState([]);
    const [ maids, setMaids ] = useState();

    useEffect(() => {
        var yesterday = moment().subtract(1,'days').startOf('day').format("YYYY-MM-DD");
        fetchList(yesterday);
        isFirstRender.current = false;
    }, [])

    useEffect(() => {
        var maidList = [];
        for (var cleaning of allCleaning) {
            var check = true;
            for (var maid of maidList) {
                if (cleaning.maid.id === maid.id) {
                    check = false;
                }
            }
            if (check) {
                maidList.push(cleaning.maid);
            }
        }
        setMaids(maidList);
    }, [allCleaning])

    const fetchList = (date) => {
        fetch('/api/cleaning/getByDate/' + date, defaultRequestOptions())
        .then((response) => response.json())
        .then(values => {
            console.log(values);
            setAllCleaning(values);
        });
    }

    const onTabChange = (key) => {
        console.log(key);
    };

    const onDateChange = (date, dateString) => {
        fetchList(dateString);
      };

    return(
        <>
        <div style={{float:'none', width: '100vh'}}></div>
        <DatePicker style={{float: 'right', marginRight:'50px'}}
            format="YYYY-MM-DD"
            disabledDate={disabledDate}
            onChange={onDateChange}
            defaultValue={moment().subtract(1,'days')}
            allowClear={false}
            />
        <Tabs type="card" onChange={onTabChange} style={{width: '100%'}}>
        { maids ? 
            maids.length ?
                maids.map((maid) => (
                    <>
                    <TabPane tab={maid.firstName + " " + maid.lastName} key={maid.id}>
                        <MaidWorkingListPdf maid={maid} allCleaning={allCleaning} />
                    </TabPane>
                    </>
                    ))
                :  <><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></>
            : <></> }
        </Tabs>
        </>
    )
}