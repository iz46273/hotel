import { Button, Form, Input, InputNumber, message, Select, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { TbUsers } from 'react-icons/tb';
import 'react-phone-number-input/style.css';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

const { Option } = Select;

const formItemLayout = {
    labelCol: {xs: {span: 12}, sm: {span: 6}},
    wrapperCol: {xs: {span: 12}, sm: {span: 8}}
};

const tailFormItemLayout = {
    wrapperCol: {xs: {span: 24, offset: 0}, sm: {span: 16, offset: 8}}
};

export default function UpdateReservation({reservation, setPage}) {

    const [form] = Form.useForm();
    const [pansions, setPansions] = useState([]);
    const [adults, setAdults] = useState(reservation.adults);
    const [children, setChildren] = useState(reservation.children);
    const currentUser = JSON.parse(localStorage.getItem("user"));
    const [receptionist, setReceptionist] = useState();

    useEffect(() => {
        fetchPansions();
        fetchUser();
    }, [])

    const fetchUser = () => {
        fetch('api/employees/' + currentUser.id, defaultRequestOptions())
        .then((response) => response.json())
        .then(rec => {
            setReceptionist(rec);
        });
    }

    useEffect(() => {
        form.setFieldsValue({
            id: reservation.id,
            room: reservation.room,
            checkInDate: reservation.checkInDate,
            checkOutDate: reservation.checkOutDate,
            email: reservation.guest.email,
            adults: reservation.adults,
            children: reservation.children,
            babies: reservation.babies,
            pansion: reservation.pansion.id,
            receptionNote: reservation.receptionNote,
            housekeepingNote: reservation.housekeepingNote,
        });
    }, [reservation])

    const fetchPansions = () => {
        fetch('/api/pansions', defaultRequestOptions())
        .then((response) => response.json())
        .then(pansionList => {
            setPansions(pansionList);
        });
    }

    const onFinish = (values) => {

        var pansion = pansions.find(x => x.id === values.pansion);
        values.pansion = pansion;

        values.id = reservation.id;
        values.createdDate = reservation.createdDate;
        values.createdTime = reservation.createdTime;
        values.status = reservation.status;
        values.totalPrice = reservation.totalPrice;
        values.checkInTime = reservation.checkInTime;
        values.checkOutTime = reservation.checkOutTime;
        values.receptionist = receptionist;

        console.log(values);

        fetch('/api/guests/checkIfExists/' + values.email, defaultRequestOptions())
        .then((response) => response.json())
        .then(check => {
            if (!check) {
                message.error("Guest with this email does not exist!")
            }
            else if (check) {
                fetch('/api/guests/getByEmail/' + values.email, defaultRequestOptions())
                .then((response) => response.json())
                .then(guest => {
                    values.guest = guest;
                    UpdateReservation(values);
                });
            }
        });
    };

    const UpdateReservation = (values) => {
        try {
            fetch('api/reservations/update', defaultRequestOptions("POST", values))
            .then((response) => {
                if (response.ok) {
                    message.success('Changes are saved successfuly.');
                    setPage('all');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    return (
        <>
            <div style={{float: 'none', width: '100%', height:'50px', background: 'lightskyblue', marginBottom: '15px'}}>
                <Space direction="horizontal" size={12} style={{float: 'left', margin: '10px', color: 'white', fontSize:'150%'}}>
                    Update reservation
                </Space>
            </div>
            <div style={{overflow: 'auto', marginBottom:'200px', height:'85%', float: 'none', marginLeft: '100px'}}>

            <Form
                {...formItemLayout}
                form={form}
                name="reservation"
                requiredMark="optional"
                onFinish={onFinish}
                initialValues={{
                prefix: '86',
                vip: 'NO',
                pansion: 'NO'
                }}
                scrollToFirstError
            >
                <Form.Item label="Room" name="room" rules={[{required: true}]}>
                    <input type='hidden'></input>
                    <>{reservation.room.id}</>
                </Form.Item>
                <Form.Item label="Check-in date" name="checkInDate" rules={[{required: true}]}>
                    <input type='hidden'></input>
                    <>{reservation.checkInDate}</>
                </Form.Item>
                <Form.Item label="Check-out date" name="checkOutDate" rules={[{required: true}]}>
                    <input type='hidden'></input>
                    <>{reservation.checkOutDate}</>
                </Form.Item>
                <Form.Item name={["email"]} label="Guest e-mail" rules={[
                    {
                    type: 'email',
                    message: 'The input is not valid e-mail.',
                    },
                    {
                    required: true,
                    message: 'E-mail cannot be empty.',
                    },
                ]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Number of adults" name="adults" rules={[{required: true, message: 'Number of adults cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />} min={1} max={reservation.room.capacity - children} value={adults} onChange={(code) => setAdults(code)} />
                </Form.Item>
                <Form.Item label="Number of children" name="children" rules={[{required: true, message: 'Number of children cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />} min={0} max={reservation.room.capacity - adults} value={children} onChange={(code) => setChildren(code)} />
                </Form.Item>
                <Form.Item label="Number of babies" name="babies" rules={[{required: true, message: 'Number of babies cannot be empty.'}]}>
                    <InputNumber addonBefore={<TbUsers />}  min={0} max={2} />
                </Form.Item>
                <Form.Item label="Meal plan" name="pansion" rules={[{required: true}]}>
                    <Select>
                        {pansions ? 
                            pansions.length ?
                                pansions.map((pansion) => (
                                    <Option value={pansion.id}>{pansion.name}</Option>
                                ))
                            :  <></>
                        : <></>}
                    </Select>
                </Form.Item>
                <Form.Item name={['receptionNote']} label="Notes for reception" rules={[{ required: false }]}>
                    <Input.TextArea placeholder="notes" />
                </Form.Item>
                <Form.Item name={['housekeepingNote']} label="Notes for housekeeping" rules={[{ required: false }]}>
                    <Input.TextArea placeholder="notes" />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                <Button type="default" onClick={() => setPage('all')}>
                    Go back
                </Button>&nbsp;&nbsp;&nbsp;&nbsp;
                <Button type="primary" htmlType="submit">
                    Submit changes
                </Button>
                </Form.Item>
            </Form>
            </div>
        </>
    )
}