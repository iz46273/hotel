import { Empty } from 'antd';
import React, { useEffect, useState } from "react";
import PrintButton from "./PrintButton";

export default function MaidWorkingListPdf({maid, allCleaning}) {
    const [cleaning, setCleaning] = useState([]);
    const [dataSource, setDataSource] = useState();
    const [date, setDate] = useState();
    const [time, setTime] = useState();
    const [cleaningDate, setCleaningDate] = useState();

    useEffect(() => {
        var data = [];
        for (var cle of cleaning) {
            var key = cle.reservation.room.id;
            var room = cle.reservation.room;
            var roomId;
            if (cle.arrival)
                roomId = cle.reservation.room.id + " !";
            else
                roomId = cle.reservation.room.id;
            var status;
            if (cle.arrival)
                status = "Arrival";
            if (cle.dueOut)
                status = "Due Out";
            if (cle.departured)
                status = "Departured";
            if (cle.stayover)
                status = "Stayover";
            if (cle.vacant)
                status = "Vacant";
            if (cle.reservation.guest)
                var vip = cle.reservation.guest.vip.id;
            else
                var vip = undefined;
            var checkInDate = cle.reservation.checkInDate;
            var checkInTime = cle.reservation.checkInTime;
            var checkOutDate = cle.reservation.checkOutDate;
            var checkOutTime = cle.reservation.checkOutTime;
            var guests = cle.reservation.adults + cle.reservation.children + cle.reservation.babies;
            var note = cle.reservation.housekeepingNote;
            var maidNote = cle.maidNote;
            var row = {
                key: key,
                room: room,
                roomId: roomId,
                status: status,
                vip: vip,
                checkInDate: checkInDate,
                checkInTime: checkInTime,
                checkOutDate: checkOutDate,
                checkOutTime: checkOutTime,
                guests: guests,
                note: note,
                maidNote, maidNote
            };
            data.push(row);
            setCleaningDate(cle.date);
        }
        setDataSource(data);
    }, [cleaning])

    useEffect(() => {
        var cleaningList = [];
        for (var cleaning of allCleaning) {
            if (cleaning.maid.id === maid.id) {
                cleaningList.push(cleaning);
            }
        }
        setCleaning(cleaningList);

        var currentdate = new Date();
        var dateTemp = currentdate.getFullYear() + "-" + (currentdate.getMonth()+1) + "-" + currentdate.getDate();
        var timeTemp = currentdate.getHours() + ":"  + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        setDate(dateTemp);
        setTime(timeTemp)
    }, [])
    
    const tableRef = React.useRef();

  return (
    <>
        <div ref={tableRef}>
            <div style={{display: 'grid', gridTemplateColumns: 'auto auto', margin: '10px'}}>
                <div class="grid-item"><a style={{fontWeight: 'bold', color: 'black'}}>Maid:</a> {maid.firstName} {maid.lastName}</div>
                <div class="grid-item"><a style={{fontWeight: 'bold', color: 'black'}}>Date:</a> {date}</div>
                <div class="grid-item"><a style={{fontWeight: 'bold', color: 'black'}}>Cleaning date:</a> {cleaningDate}</div>
                <div class="grid-item"><a style={{fontWeight: 'bold', color: 'black'}}>Time:</a> {time}</div>
            </div>
            <table style={{
                borderColor: 'grey',
                width: '100%',
                display: 'table',
                borderSpacing: '0',
                borderCollapse: 'collapse',
                color: 'rgba(0, 0, 0, 0.87)',
                transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                backgroundColor:'#fff',
                boxShadow: '0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12)',
                fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
            }}>
            <tbody>
                <tr style={{borderBottom: '1px solid rgba(224, 224, 224, 1)'}}>
                    <th rowSpan={2} style={{textAlign: 'center'}}>Room</th>
                    <th rowSpan={2} style={{textAlign: 'center'}}>Status</th>
                    <th rowSpan={2} style={{textAlign: 'center'}}>VIP</th>
                    <th colSpan={2} style={{textAlign:'center'}}>Check-in</th>
                    <th colSpan={2} style={{textAlign:'center'}}>Check-out</th>
                    <th rowSpan={2} style={{textAlign: 'center'}}>Guests</th>
                    <th rowSpan={2} style={{textAlign: 'center'}}>Guest note</th>
                    <th rowSpan={2} style={{textAlign: 'center'}}>Maid note</th>
                </tr>
                <tr style={{borderBottom: '1px solid rgba(224, 224, 224, 1)'}}>
                    <th style={{textAlign: 'center'}}>Date</th>
                    <th style={{textAlign: 'center'}}>Time</th>
                    <th style={{textAlign: 'center'}}>Date</th>
                    <th style={{textAlign: 'center'}}>Time</th>
                </tr>
                { dataSource ? 
                    dataSource.length ?
                        dataSource.map((data) => (
                            <>
                            <tr style={{borderBottom: '1px solid rgba(224, 224, 224, 1)'}}>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.roomId}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.status}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.vip}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.checkInDate}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.checkInTime}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.checkOutDate}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.checkOutTime}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.guests}</td>
                                <td style={{textAlign: 'center', padding: '8px'}}>{data.note}</td>
                                <td style={{textAlign: 'center', padding: '8px', width:'150px'}}></td>
                            </tr>
                            </>
                            ))
                        :  <><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></>
                    : <><Empty image={Empty.PRESENTED_IMAGE_SIMPLE} /></> }
            </tbody>
        </table>
      </div><br></br>
      <div style={{textAlign: 'center', width: '100%'}}>
        <PrintButton refsToPrint={[tableRef]} />
      </div>
    </>
  );
}