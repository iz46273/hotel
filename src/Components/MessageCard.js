import { Comment, message, Tooltip } from 'antd';
import React, { useEffect, useState } from 'react';
import { BiUser } from 'react-icons/bi';
import defaultRequestOptions from '../helpers/defaultRequestOptions';

export default function MessageCard({myMessage, newMessage, refresh}) {

    const [msgTime, setMsgTime] = useState('');

    useEffect(() => {
        var dateTime = myMessage.time;
        var arr = dateTime.split("T");
        var date = arr[0];
        var time = arr[1];

        date = date.replace("-", "/");
        date = date.replace("-", "/");
        var timeArr = time.split(":");

        setMsgTime(date + " at " + timeArr[0] + ":" + timeArr[1]);
    }, [])

    const reply = () => {
        newMessage(myMessage);
    }

    const confirm = () => {
        try {
            fetch('api/messages/read/' + myMessage.id, defaultRequestOptions())
            .then((response) => {
                if (response.ok) {
                    refresh();
                    message.success('Message seen.');
                }
                else {
                    message.error('Something went wrong. Please try again.')
                }
            })
        } catch (err) {
            message.error('Cannot make connection to server. Please try again.')
        }
    }

    const actions = [
        <span onClick={confirm} key="comment-basic-reply-to">OK</span>,
        <span onClick={reply} key="comment-basic-reply-to">Reply</span>,
    ];

    return(
        <>
        <Comment
            style={{border: '1px solid lightgrey', borderRadius: '15px', padding: '10px'}}
            actions={actions}
            size="large"
            author={<a>
                <Tooltip title={myMessage.from.position}>
                    <BiUser /> {myMessage.from.firstName} {myMessage.from.lastName}
                </Tooltip>
                </a>}
            content={
                <p>
                {myMessage.message}
                </p>
            }
            datetime={
                <span>{msgTime}</span>
            }
            />
        </>
    )
}