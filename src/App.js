import React, { useEffect, useState } from 'react';
import './App.css';
import Login from './Components/Login';
import defaultRequestOptions from './helpers/defaultRequestOptions';
import Admin from './Pages/Admin';
import Housekeeping from './Pages/Housekeeping';
import Reception from './Pages/Reception';

function App() {
  const [currentUser, setCurrentUser] = useState();

  useEffect(() => {
    setCurrentUser(JSON.parse(localStorage.getItem("user")));
  }, [])

  useEffect(() => {
    console.log(currentUser);
    if (currentUser != undefined)
      fetch('/api/data/dailyInit', defaultRequestOptions());
  }, [currentUser])

  const logout = () => {
    localStorage.removeItem("user");
    setCurrentUser(undefined);
  }

  if(!currentUser) {
    return <Login setCurrentUser={setCurrentUser} />
  }

  if(currentUser.position == "RECEPTION") {
    console.log("reception")
    return <Reception logout={logout} />
  }

  if(currentUser.position == "HOUSEKEEPING") {
    return <Housekeeping logout={logout} />
  }

  if(currentUser.position == "ADMINISTRATION") {
    return <Admin logout={logout} />
  }
}

export default App;