import React, { Component } from 'react';

export default class PansionService extends Component {

    state = {
        pansions: []
    }

    fetchPansions = () => {
        fetch('/api/pansions')
        .then((response) => response.json())
        .then(pansionList => {
            this.setState({ pansions: pansionList });
        });
    }

    render(){
        return (
            <>
            <button onClick={this.fetchPansions}>Load</button>
            <ul>
                {this.state.pansions.map((pansion) => (
                    <li key={pansion.id}>{pansion.price}</li>
                ))}
            </ul>
            </>
        )
    }

    componentDidMount() {
        fetch('/api/pansions')
        .then((response) => response.json())
        .then(pansionList => {
            this.setState({ pansions: pansionList });
        });
    }

}