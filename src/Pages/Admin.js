import { Layout, Menu } from 'antd';
import React, { useEffect, useState } from 'react';
import { BiHome, BiUser } from 'react-icons/bi';
import { BsCalendarWeek, BsJournalText, BsViewList } from 'react-icons/bs';
import { IoLogOutOutline, IoSettingsOutline } from 'react-icons/io5';
import { MdOutlineBedroomParent } from 'react-icons/md';
import { TbUsers } from 'react-icons/tb';
import AdminDashboard from '../Components/AdminDashboard';
import AllEmployees from '../Components/AllEmployees';
import AllRooms from '../Components/AllRooms';
import InitPansions from '../Components/InitPansions';
import InitRoomCategories from '../Components/InitRoomCategories';
import NewEmployee from '../Components/NewEmployee';
import NewRoom from '../Components/NewRoom';

const { Header, Content, Footer, Sider } = Layout;

const items1 = [
  {
    key: "home",
    label: "Home",
    icon: <BiHome />,
  },
  {
    key: "logout",
    label: "Log out",
    icon: <IoLogOutOutline />,
  },
]

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items2 = [
    getItem('Overview', 'overview', <BsViewList />),
    getItem('Rooms', 'rooms', <MdOutlineBedroomParent />, [
      getItem('All', 'allRooms'),
      getItem('New', 'newRoom'),
      getItem('Categories', 'roomCategories'),
    ]),
    getItem('Emplolyees', 'employees', <MdOutlineBedroomParent />, [
        getItem('All', 'allEmployees'),
        getItem('New', 'newEmployee'),
      ]),
    getItem('Reservations', 'reservations', <BsJournalText />, [
      getItem('Meal plans', 'pansions'),
    ]),
];

const rootSubmenuKeys = ['overview', 'employees', 'rooms', 'reservations', 'guests', 'settings'];

export default function App({logout}) {
  const [value, setValue] = useState('');
  const [openKeys, setOpenKeys] = useState();

  useEffect(() => {
    console.log(value);
  }, [])
  
  const handleSelect=(e)=>{
    setValue(e)
  }

  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);

    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const handleMainMenuSelect = (e) => {
    if (e.key == "logout") {
      logout();
    }
  }

  return (
    <Layout
    style={{
      height: '100vh',
    }}
    >
      <Header className="header"
        style={{
          backgroundColor: 'white',
        }}
      >
        <Menu 
        mode="horizontal" 
        defaultSelectedKeys={['home']} 
        items={items1} 
        onSelect={handleMainMenuSelect}
          style={{
            width: '100%',
          }}
        />
      </Header>
      <Content
        style={{
          padding: '0 50px',
        }}
      >
        <Layout
          className="site-layout-background"
          style={{
            padding: '24px 0',
            height: '100%',
          }}
        >
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['overview']}
              onSelect={handleSelect}
              openKeys={openKeys}
              onOpenChange={onOpenChange}
              style={{
                height: '100%',
              }}
              items={items2}
            />
          </Sider>
          <Content
            style={{
              padding: '0 24px',
              minHeight: 280,
            }}
          >
            {value.key == "overview" || value == '' ?
                  <AdminDashboard/>
                : <></>
            }
            {value.key == "allRooms" ?
                  <AllRooms />
                : <></>
            }
            {value.key == "newRoom" ?
                  <NewRoom />
                : <></>
            }
            {value.key == "allEmployees" ?
                  <AllEmployees />
                : <></>
            }
            {value.key == "newEmployee" ?
                  <NewEmployee />
                : <></>
            }
            {value.key == "roomCategories" ?
                <InitRoomCategories />
                : <></>
            }
            {value.key == "pansions" ?
                <InitPansions />
                : <></>
            }
          </Content>
        </Layout>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
          float: 'none',
        }}
      >
        Hotel management application ©2022 Created by Ivana Zeljic, IT student of Split University
      </Footer>
    </Layout>
  );
}