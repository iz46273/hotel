import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { BiHome, BiUser } from 'react-icons/bi';
import { BsCalendarWeek, BsJournalText, BsViewList } from 'react-icons/bs';
import { IoLogOutOutline, IoSettingsOutline } from 'react-icons/io5';
import Cleaning from '../Components/Cleaning';
import { CleaningArchive } from '../Components/CleaningArchive';
import HousekeepingDashboard from '../Components/HousekeepingDashboard';

const { Header, Content, Footer, Sider } = Layout;

const items1 = [
  {
    key: "home",
    label: "Home",
    icon: <BiHome />,
  },
  {
    key: "logout",
    label: "Log out",
    icon: <IoLogOutOutline />,
  },
]

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items2 = [
  getItem('Overview', 'overview', <BsViewList />),
  getItem('Cleaning', 'cleaning', <BsJournalText />),
  getItem('Archive', 'archive', <BsJournalText />),
];

const rootSubmenuKeys = ['overview', 'cleaning', 'archive', 'maids', 'settings'];

export default function App({logout}) {
  const [value,setValue]=useState('');
  const [openKeys, setOpenKeys] = useState();
  
  const handleSelect=(e)=>{
    setValue(e)
  }

  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);

    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const handleMainMenuSelect = (e) => {
    if (e.key == "logout") {
      logout();
    }
  }

  return (
    <Layout
    style={{
      height: '100vh',
    }}
    >
      <Header className="header"
        style={{
          backgroundColor: 'lightskyblue',
        }}
      >
        <Menu 
          mode="horizontal" 
          defaultSelectedKeys={['home']} 
          items={items1}
          onSelect={handleMainMenuSelect}
          style={{
            width: '100%',
          }}
        />
      </Header>
      <Content
        style={{
          padding: '0 50px',
        }}
      >
        <Layout
          className="site-layout-background"
          style={{
            padding: '24px 0',
            height: '100%',
          }}
        >
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['overview']}
              onSelect={handleSelect}
              openKeys={openKeys}
              onOpenChange={onOpenChange}
              style={{
                height: '100%',
              }}
              items={items2}
            />
          </Sider>
          <Content
            style={{
              padding: '0 24px',
              minHeight: 280,
            }}
          >
            {value.key == "overview" || value == '' ?
                  <HousekeepingDashboard />
                : <></>
            }
            {value.key == "cleaning" ?
                  <Cleaning />
                : <></>
            }
            {value.key == "archive" ?
                  <CleaningArchive />
                : <></>
            }
          </Content>
        </Layout>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
          float: 'none',
        }}
      >
        Hotel management application ©2022 Created by Ivana Zeljic, IT student of Split University
      </Footer>
    </Layout>
  );
}