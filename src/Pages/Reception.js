import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { BiHome, BiUser } from 'react-icons/bi';
import { BsCalendarWeek, BsJournalText, BsViewList } from 'react-icons/bs';
import { IoLogOutOutline, IoSettingsOutline } from 'react-icons/io5';
import { MdOutlineBedroomParent } from 'react-icons/md';
import { TbUsers } from 'react-icons/tb';
import AllGuests from '../Components/AllGuests';
import AllReservations from '../Components/AllReservations';
import ReceptionDashboard from '../Components/ReceptionDashboard';
import Reservations from '../Components/Reservations';
import Rooms from '../Components/Rooms';

const { Header, Content, Footer, Sider } = Layout;

const items1 = [
  {
    key: "home",
    label: "Home",
    icon: <BiHome />,
  },
  {
    key: "logout",
    label: "Log out",
    icon: <IoLogOutOutline />,
  },
]

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items2 = [
  getItem('Overview', 'overview', <BsViewList />),
  getItem('Rooms', 'rooms', <MdOutlineBedroomParent />, [
    getItem('All', 'allRooms'),
    getItem('Available Now', 'availableNow'),
    getItem('Available Today', 'availableToday'),
  ]),
  getItem('Reservations', 'reservations', <BsJournalText />, [
    getItem('All', 'allReservations'),
    getItem('Arrival', 'arrival'),
    getItem('Departure', 'departure'),
    getItem('Stayover', 'stayover'),
  ]),
  getItem('Guests', 'guests', <TbUsers />, [
    getItem('All', 'allGuests'),
  ]),
];

const rootSubmenuKeys = ['overview', 'rooms', 'reservations', 'guests', 'calendar', 'settings'];

export default function App({logout}) {
  const [value,setValue]=useState('');
  const [openKeys, setOpenKeys] = useState();
  
  const handleSelect=(e)=>{
    setValue(e)
  }

  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);

    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const handleMainMenuSelect = (e) => {
    console.log(e);
    if (e.key == "logout") {
      logout();
    }
  }

  return (
    <Layout
    style={{
      height: '100vh',
    }}
    >
      <Header className="header"
        style={{
          backgroundColor: 'lightskyblue',
        }}
      >
        <Menu mode="horizontal"
          defaultSelectedKeys={['home']}
          items={items1}
          onSelect={handleMainMenuSelect}
          style={{
            width: '100%',
          }}
        />
      </Header>
      <Content
        style={{
          padding: '0 50px',
        }}
      >
        <Layout
          className="site-layout-background"
          style={{
            padding: '24px 0',
            height: '100%',
          }}
        >
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['overview']}
              onSelect={handleSelect}
              openKeys={openKeys}
              onOpenChange={onOpenChange}
              style={{
                height: '100%',
              }}
              items={items2}
            />
          </Sider>
          <Content
            style={{
              padding: '0 24px',
              minHeight: 280,
            }}
          >
            {value.key == "overview" || value == '' ?
                  <ReceptionDashboard />
                : <></>
            }
            {value.key == "allRooms" ?
              <Rooms view="allrooms" /> : <></>
            }
            {value.key == "availableToday" ?
              <Rooms view="availableToday"/> : <></>
            }
            {value.key == "availableNow" ?
              <Rooms view="availableNow" /> : <></>
            }
            {value.key == "allReservations" ?
              <AllReservations /> : <></>
            }
            {value.key == "arrival" ?
              <Reservations view="arrival" /> : <></>
            }
            {value.key == "departure" ?
              <Reservations view="dueOut" /> : <></>
            }
            {value.key == "stayover" ?
              <Reservations view="stayover" /> : <></>
            }
            {value.key == "allGuests" ?
              <AllGuests /> : <></>
            }
          </Content>
        </Layout>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
          float: 'none',
        }}
      >
        Hotel management application ©2022 Created by Ivana Zeljic, IT student of Split University
      </Footer>
    </Layout>
  );
}